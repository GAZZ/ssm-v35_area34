package edu.demo;

import edu.uc.service.TransService;
import edu.util.SpringUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;

public class TransTxTest {

    private Long userA = 1L;
    private Long userB = 2L;
    private Long initMoney = 1000L;
    private Long transMoney = 500L;

    private TransService transService = null;

    @Before
    public void init() {
        transService = SpringUtil.getBean("transService", TransService.class);

        transService.showMoney(userA, userB);
        transService.testTxInit(userA, userB, initMoney);
        transService.showMoney(userA, userB);
    }

    @Test
    public void run() throws SQLException {
        System.out.println("Start......");

        transService.showMoney(userA, userB);
        boolean isOK = transService.testTxYes(userA, userB, transMoney);
        transService.showMoney(userA, userB);

        Assert.assertTrue(isOK);

        System.out.println("End......");
    }

}
