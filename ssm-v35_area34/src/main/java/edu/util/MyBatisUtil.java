package edu.util;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

public class MyBatisUtil {

    private MyBatisUtil() {
    }

    private static class InnerFun {
        public static SqlSessionFactory factory;

        static {
            System.out.println("init MyBatis==========");
            try {
                InputStream is = Resources.getResourceAsStream("mybatis-config-30.xml");
                factory = new SqlSessionFactoryBuilder().build(is);
            } catch (IOException e) {
//                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }
    }

    public static SqlSession createSqlSession(boolean autoCommit) {
        return InnerFun.factory.openSession(autoCommit);
    }

    public static SqlSession createSqlSession() {
        return InnerFun.factory.openSession(false);
    }

    public static void closeSqlSession(SqlSession sqlSession) {
        if (sqlSession != null) {
            sqlSession.close();
        }
    }
}
