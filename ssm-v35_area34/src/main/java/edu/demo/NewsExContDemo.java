package edu.demo;

import com.liuvei.common.RandFun;
import edu.uc.bean.NewsExCont;
import edu.uc.service.NewsExContService;
import edu.util.SpringUtil;

import java.util.List;

public class NewsExContDemo {

    private  static NewsExContService newsExContService = SpringUtil.getBean("newsExContService", NewsExContService.class);

    private static NewsExCont bean;

    public static void main(String[] args) {

        Integer num = RandFun.rand4Num();

        bean = new NewsExCont();
        bean.setNewsId(1235086L);
        bean.setContent("content_s24_" + num);

        list();
        insert();
        load();
        loadByNewsId();
        update();
        delete();
        deleteByNewsId();
        count();
        countByNewsId();
        pager();
        pagerByNewsId();
    }

    private static void show(NewsExCont item) {
        System.out.println(item.getId() + "\t" + item.getContent());
    }

    private static void insert() {
        Long result = newsExContService.insert(bean);
        if (result > 0) {
            System.out.println("OK");
        } else {
            System.out.println("Not OK");
        }
    }

    private static void delete() {
        Long result = newsExContService.delete(bean.getId());
        if (result > 0) {
            System.out.println("OK");
        } else {
            System.out.println("Not OK");
        }
    }

    private static void deleteByNewsId() {
        Long result = newsExContService.deleteByNewsId(bean.getNewsId());
        if (result > 0) {
            System.out.println("OK");
        } else {
            System.out.println("Not OK");
        }
    }

    private static void update() {
        Integer num = RandFun.rand4Num();

        bean.setNewsId(999L);
        bean.setContent("content_s24_" + num);

        Long result = newsExContService.update(bean);
        if (result > 0) {
            System.out.println("OK");
        } else {
            System.out.println("Not OK");
        }
    }

    private static void list() {
        List<NewsExCont> list = newsExContService.list();
        for (NewsExCont newsExCont : list) {
            show(newsExCont);
        }
    }

    private static void load() {
        NewsExCont newsExCont = newsExContService.load(bean.getId());
        show(newsExCont);
    }

    private static void loadByNewsId() {
        NewsExCont newsExCont = newsExContService.loadByNewsId(bean.getNewsId());
        show(newsExCont);
    }

    private static void count() {
        Long result = newsExContService.count();
        System.out.println(result);
    }

    private static void countByNewsId() {
        Long result = newsExContService.countByNewsId(bean.getNewsId());
        System.out.println(result);
    }

    private static void pager() {
        List<NewsExCont> list = newsExContService.pager(1L, 5L);
        for (NewsExCont newsExCont : list) {
            show(newsExCont);
        }
    }

    private static void pagerByNewsId() {
        List<NewsExCont> list = newsExContService.pagerByNewsId(bean.getNewsId(), 1L, 5L);
        for (NewsExCont newsExCont : list) {
            show(newsExCont);
        }
    }
}
