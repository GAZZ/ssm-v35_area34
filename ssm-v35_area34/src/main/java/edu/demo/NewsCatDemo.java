package edu.demo;

import com.liuvei.common.RandFun;
import edu.uc.bean.NewsCat;
import edu.uc.service.NewsCatService;
import edu.util.SpringUtil;

import java.util.List;

public class NewsCatDemo {

    private static NewsCatService newsCatService = SpringUtil.getBean("newsCatService", NewsCatService.class);

    private static NewsCat bean;

    public static void main(String[] args) {

        Integer num = RandFun.rand4Num();

        bean = new NewsCat();
        bean.setCatName("catName_s24_" + num);
        bean.setCatDesc("catDesc_s24_" + num);

        list();
        insert();
        load();
        loadByCatName();
        update();
        delete();
        count();
        countByCatName();
        pager();
        pagerByCatName();
    }

    private static void show(NewsCat item) {
        System.out.println(item.getCatId() + "\t" + item.getCatName());
    }

    private static void list() {
        List<NewsCat> list = newsCatService.list();
        for (NewsCat newsCat : list) {
            show(newsCat);
        }
    }

    private static void load() {
        NewsCat newsCat = newsCatService.load(bean.getCatId());
        show(newsCat);
    }

    private static void loadByCatName() {
        NewsCat newsCat = newsCatService.loadByCatName(bean.getCatName());
        show(newsCat);
    }

    private static void insert() {
        Long result = newsCatService.insert(bean);
        if (result > 0) {
            System.out.println("OK");
        } else {
            System.out.println("Not OK");
        }
    }

    private static void delete() {
        Long result = newsCatService.delete(bean.getCatId());
        if (result > 0) {
            System.out.println("OK");
        } else {
            System.out.println("Not OK");
        }
    }

    private static void update() {
        Integer num = RandFun.rand4Num();

        bean.setCatName("catName_s24_" + num);
        bean.setCatDesc("catDesc_s24_" + num);

        Long result = newsCatService.update(bean);
        if (result > 0) {
            System.out.println("OK");
        } else {
            System.out.println("Not OK");
        }
    }

    private static void count() {
        Long result = newsCatService.count();
        System.out.println(result);
    }

    private static void countByCatName() {
        Long result = newsCatService.countByCatName(bean.getCatName());
        System.out.println(result);
    }

    private static void pager() {
        List<NewsCat> list = newsCatService.pager(1L, 5L);
        for (NewsCat newsCat : list) {
            show(newsCat);
        }
    }

    private static void pagerByCatName() {
        List<NewsCat> list = newsCatService.pagerByCatName(bean.getCatName(), 1L, 5L);
        for (NewsCat newsCat : list) {
            show(newsCat);
        }
    }
}
