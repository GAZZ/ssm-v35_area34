package edu.demo;

import com.liuvei.common.RandFun;
import edu.uc.bean.Member;
import edu.uc.service.MemberService;
import edu.util.SpringUtil;

import java.util.List;

public class MemberDemo {

    private static MemberService memberService = SpringUtil.getBean("memberService", MemberService.class);

    private static Member bean;

    public static void main(String[] args) {

        Integer num = RandFun.rand4Num();

        bean = new Member();
        bean.setUserName("userName_s24_" + num);
        bean.setUserPass("userPass_s24_" + num);
        bean.setNickName("nickName_s24_" + num);
        bean.setEmail("email_s24_" + num + "@qq.com");

        list();
        insert();
        load();
        update();
        loadByUserName();
        loadByNickName();
        loadByEmail();
        delete();
        count();
        countByUserName();
        pager();
        pagerByUserName();
    }

    private static void show(Member item) {
        System.out.println(item.getUserId() + "\t" + item.getUserName() + "\t" + item.getNickName());
    }

    private static void insert() {
        Long result = memberService.insert(bean);
        if (result > 0) {
            System.out.println("OK");
        } else {
            System.out.println("Not OK");
        }
    }

    private static void delete() {
        Long result = memberService.delete(bean.getUserId());
        if (result > 0) {
            System.out.println("OK");
        } else {
            System.out.println("Not OK");
        }
    }

    private static void update() {
        Integer num = RandFun.rand4Num();

        bean.setUserName("userName_s24_" + num);
        bean.setUserPass("userPass_s24_" + num);
        bean.setNickName("nickName_s24_" + num);
        bean.setEmail("email_s24_" + num + "@qq.com");

        Long result = memberService.update(bean);
        if (result > 0) {
            System.out.println("OK");
        } else {
            System.out.println("Not OK");
        }
    }

    private static void list() {
        List<Member> list = memberService.list();
        for (Member member : list) {
            show(member);
        }
    }

    private static void load() {
        Member member = memberService.load(bean.getUserId());
        show(member);
    }

    private static void loadByUserName() {
        Member member = memberService.loadByUserName(bean.getUserName());
        show(member);
    }

    private static void loadByNickName() {
        Member member = memberService.loadByNickName(bean.getNickName());
        show(member);
    }

    private static void loadByEmail() {
        Member member = memberService.loadByEmail(bean.getEmail());
        show(member);
    }

    private static void count() {
        Long result = memberService.count();
        System.out.println(result);
    }

    private static void countByUserName() {
        Long result = memberService.countByUserName(bean.getUserName());
        System.out.println(result);
    }

    private static void pager() {
        List<Member> list = memberService.pager(1L, 5L);
        for (Member member : list) {
            show(member);
        }
    }

    private static void pagerByUserName() {
        List<Member> list = memberService.pagerByUserName(bean.getUserName(), 1L, 5L);
        for (Member member : list) {
            show(member);
        }
    }
}
