package edu.demo;

import com.liuvei.common.RandFun;
import edu.uc.bean.News;
import edu.uc.service.NewsService;
import edu.util.SpringUtil;

import java.util.List;

public class NewsDemo {

    private static NewsService newsService = SpringUtil.getBean("newsService", NewsService.class);

    private static News bean;

    public static void main(String[] args) {

        Integer num = RandFun.rand4Num();

        bean = new News();
        bean.setCatId(1L);
        bean.setTitle("title_s24_" + num);
        bean.setAuthor("author_s24_" + num);
        bean.setSummary("summary_s24_" + num);

        list();
        insert();
        load();
        loadByTitle();
        update();
        delete();
        count();
        countByCatId();
        countByAuthor();
        countByTitle();
        pager();
        pagerByTitle();
    }

    private static void show(News item) {
        System.out.println(item.getNewsId() + "\t" + item.getTitle());
    }

    private static void list() {
        List<News> list = newsService.list();
        for (News news : list) {
            show(news);
        }
    }

    private static void load() {
        News news = newsService.load(bean.getNewsId());
        show(news);
    }

    private static void loadByTitle() {
        System.out.println();
        News news = newsService.loadByTitle(bean.getTitle());
        show(news);
    }

    private static void insert() {
        Long result = newsService.insert(bean);
        if (result > 0) {
            System.out.println("OK");
        } else {
            System.out.println("Not OK");
        }
    }

    private static void delete() {
        Long result = newsService.delete(bean.getNewsId());
        if (result > 0) {
            System.out.println("OK");
        } else {
            System.out.println("Not OK");
        }
    }

    private static void update() {
        Integer num = RandFun.rand4Num();

        bean.setCatId(1L);
        bean.setTitle("title_s24_" + num);
        bean.setAuthor("author_s24_" + num);
        bean.setSummary("summary_s24_" + num);
        Long result = newsService.update(bean);

        if (result > 0) {
            System.out.println("OK");
        } else {
            System.out.println("Not OK");
        }
    }

    private static void count() {
        Long result = newsService.count();
        System.out.println(result);
    }

    private static void countByCatId() {
        Long result = newsService.countByCatId(bean.getNewsId());
        System.out.println(result);
    }

    private static void countByAuthor() {
        Long result = newsService.countByAuthor(bean.getAuthor());
        System.out.println(result);
    }

    private static void countByTitle() {
        Long result = newsService.countByTitle(bean.getTitle());
        System.out.println(result);
    }

    private static void pager() {
        List<News> list = newsService.pager(1L, 5L);
        for (News news : list) {
            show(news);
        }
    }

    private static void pagerByTitle() {
        List<News> list = newsService.pagerByTitle(bean.getTitle(), 1L, 5L);
        for (News news : list) {
            show(news);
        }
    }
}
