package edu.demo;

import com.liuvei.common.RandFun;
import edu.uc.bean.NewsComment;
import edu.uc.service.NewsCommentService;
import edu.util.SpringUtil;

import java.util.List;

public class NewsCommentDemo {

    private static NewsCommentService newsCommentService = SpringUtil.getBean("newsCommentService", NewsCommentService.class);

    private static NewsComment bean;

    public static void main(String[] args) {

        Integer num = RandFun.rand4Num();

        bean = new NewsComment();
        bean.setNewsId(1L);
        bean.setContent("content_s24_" + num);
        bean.setAuthor("author_s24_" + num);

        list();
        insert();
        load();
        update();
        delete();
        deleteByNewsId();
        count();
        countByNewsId();
        countByAuthor();
        countByAuthorAndNewsId();
        pager();
        pagerByNewsId();
        pagerByAuthor();
        pagerByAuthorAndNewsId();
    }

    private static void show(NewsComment item) {
        System.out.println(item.getId() + "\t" + item.getContent());
    }

    private static void list() {
        List<NewsComment> list = newsCommentService.list();
        for (NewsComment newsComment : list) {
            show(newsComment);
        }
    }

    private static void load() {
        NewsComment newsComment = newsCommentService.load(bean.getId());
        show(newsComment);
    }

    private static void insert() {
        Long result = newsCommentService.insert(bean);
        if (result > 0) {
            System.out.println("OK");
        } else {
            System.out.println("Not OK");
        }
    }

    private static void delete() {
        Long result = newsCommentService.delete(bean.getId());
        if (result > 0) {
            System.out.println("OK");
        } else {
            System.out.println("Not OK");
        }
    }

    private static void deleteByNewsId() {
        Long result = newsCommentService.deleteByNewsId(bean.getNewsId());
        if (result > 0) {
            System.out.println("OK");
        } else {
            System.out.println("Not OK");
        }
    }

    private static void update() {
        Integer num = RandFun.rand4Num();

        bean.setNewsId(1L);
        bean.setContent("content_s24_" + num);
        bean.setAuthor("author_s24_" + num);

        Long result = newsCommentService.update(bean);
        if (result > 0) {
            System.out.println("OK");
        } else {
            System.out.println("Not OK");
        }
    }

    private static void count() {
        Long result = newsCommentService.count();
        System.out.println(result);
    }

    private static void countByNewsId() {
        Long result = newsCommentService.countByNewsId(bean.getNewsId());
        System.out.println(result);
    }

    private static void countByAuthor() {
        Long result = newsCommentService.countByAuthor(bean.getAuthor());
        System.out.println(result);
    }

    private static void countByAuthorAndNewsId() {
        Long result = newsCommentService.countByAuthorAndNewsId(bean.getAuthor(), bean.getNewsId());
        System.out.println(result);
    }

    private static void pager() {
        List<NewsComment> list = newsCommentService.pager(1L, 5L);
        for (NewsComment newsComment : list) {
            show(newsComment);
        }
    }

    private static void pagerByNewsId() {
        List<NewsComment> list = newsCommentService.pagerByNewsId(bean.getNewsId(), 1L, 5L);
        for (NewsComment newsComment : list) {
            show(newsComment);
        }
    }

    private static void pagerByAuthor() {
        List<NewsComment> list = newsCommentService.pagerByAuthor(bean.getAuthor(), 1L, 5L);
        for (NewsComment newsComment : list) {
            show(newsComment);
        }
    }

    private static void pagerByAuthorAndNewsId() {
        List<NewsComment> list = newsCommentService.pagerByAuthorAndNewsId(bean.getAuthor(), bean.getNewsId(), 1L, 5L);
        for (NewsComment newsComment : list) {
            show(newsComment);
        }
    }
}
