package edu.uc.mapper;

import edu.uc.bean.News;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface NewsMapper extends BaseMapper<News> {

    News loadByTitle(String title);

    Long countByCatId(Long carId);

    Long countByAuthor(String author);

    Long countByTitle(String title);

    List<News> pagerByTitle(@Param("title") String title, @Param("pageNum") Long pageNum, @Param("pageSize") Long pageSize);

}
