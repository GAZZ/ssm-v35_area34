package edu.uc.mapper;

import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BaseMapper<T> {

    Long insert(T bean);

    Long delete(Long id);

    Long update(T bean);

    List<T> list();

    T load(Long id);

    Long count();

    List<T> pager(@Param("pageNum") Long pageNum, @Param("pageSize") Long pageSize);

}
