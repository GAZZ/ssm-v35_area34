package edu.uc.mapper;

import edu.uc.bean.NewsExCont;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface NewsExContMapper extends BaseMapper<NewsExCont> {

    Long deleteByNewsId(Long newsId);

    NewsExCont loadByNewsId(Long newsId);

    Long countByNewsId(Long newsId);

    List<NewsExCont> pagerByNewsId(@Param("newsId") Long newsId, @Param("pageNum") Long pageNum, @Param("pageSize") Long pageSize);

}
