package edu.uc.mapper;

import edu.uc.bean.NewsComment;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface NewsCommentMapper extends BaseMapper<NewsComment> {

    Long deleteByNewsId(Long newsId);

    Long countByNewsId(Long newsId);

    Long countByAuthor(String author);

    Long countByAuthorAndNewsId(@Param("author") String author, @Param("newsId") Long newsId);

    List<NewsComment> pagerByNewsId(@Param("newsId") Long newsId, @Param("pageNum") Long pageNum, @Param("pageSize") Long pageSize);

    List<NewsComment> pagerByAuthor(@Param("author") String author, @Param("pageNum") Long pageNum, @Param("pageSize") Long pageSize);

    List<NewsComment> pagerByAuthorAndNewsId(@Param("author") String author, @Param("newsId") Long newsId, @Param("pageNum") Long pageNum, @Param("pageSize") Long pageSize);

}
