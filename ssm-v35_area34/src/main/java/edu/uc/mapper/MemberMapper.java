package edu.uc.mapper;

import edu.uc.bean.Member;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MemberMapper extends BaseMapper<Member> {

    Member loadByUserName(String userName);

    Member loadByNickName(String nickName);

    Member loadByEmail(String email);

    Long countByUserName(String userName);

    List<Member> pagerByUserName(@Param("userName") String userName, @Param("pageNum") Long pageNum, @Param("pageSize") Long pageSize);

}
