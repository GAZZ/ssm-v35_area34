package edu.uc.mapper;

import edu.uc.bean.NewsCat;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface NewsCatMapper extends BaseMapper<NewsCat> {

    NewsCat loadByCatName(String catName);

    Long countByCatName(String catName);

    List<NewsCat> pagerByCatName(@Param("catName") String catName, @Param("pageNum") Long pageNum, @Param("pageSize") Long pageSize);

}
