package edu.uc.ui.ctrl34;

import com.liuvei.common.PagerItem;
import com.liuvei.common.SysFun;
import edu.uc.bean.Member;
import edu.uc.bean.NewsCat;
import edu.uc.service.NewsCatService;
import edu.uc.ui.ctrl02.UIConst;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping(edu.uc.ui.ctrl34.UIConst.AREAPATH)
public class NewsCatController extends CrudController {

    @Resource
    private NewsCatService newsCatService;

    private static final String BEAN_PREFIX = "/NewsCat_";

    @Override
    @RequestMapping(BEAN_PREFIX + "list")
    protected ModelAndView listView(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mView = getMView("NewsCat", "list");

        List<NewsCat> vDataList = null;

        PagerItem pagerItem = new PagerItem();
        pagerItem.parsePageSize(request.getParameter(pagerItem.getParamPageSize()));
        pagerItem.parsePageNum(request.getParameter(pagerItem.getParamPageNum()));

        Long rowCount = 0L;
        rowCount = newsCatService.count();
        pagerItem.changeRowCount(rowCount);

        vDataList = newsCatService.pager(pagerItem.getPageNum(), pagerItem.getPageSize());

        pagerItem.changeUrl(SysFun.generalUrl(request.getRequestURI(), request.getQueryString()));

        request.setAttribute("pagerItem", pagerItem);
        request.setAttribute("DataList", vDataList);

        return mView;
    }

    @Override
    @RequestMapping(BEAN_PREFIX + "listDeal")
    protected ModelAndView listDeal(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mView = getMView("NewsCat", "list");

        List<NewsCat> vDataList = null;

        String catName = request.getParameter("catName");
        request.setAttribute("catName", catName);

        PagerItem pagerItem = new PagerItem();
        pagerItem.parsePageSize(request.getParameter(pagerItem.getParamPageSize()));
        pagerItem.parsePageNum(request.getParameter(pagerItem.getParamPageNum()));

        Long rowCount = 0L;

        if (SysFun.isNullOrEmpty(catName)) {
            rowCount = newsCatService.count();
            pagerItem.changeRowCount(rowCount);
            vDataList = newsCatService.pager(pagerItem.getPageNum(), pagerItem.getPageSize());

        } else {
            rowCount = newsCatService.countByCatName(catName);
            pagerItem.changeRowCount(rowCount);
            vDataList = newsCatService.pagerByCatName(catName, pagerItem.getPageNum(), pagerItem.getPageSize());
        }

        pagerItem.changeUrl(SysFun.generalUrl(request.getRequestURI(), request.getQueryString()));

        request.setAttribute("pagerItem", pagerItem);
        request.setAttribute("DataList", vDataList);

        return mView;
    }

    @Override
    @RequestMapping(BEAN_PREFIX + "insert")
    protected ModelAndView insertView(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mView = getMView("NewsCat", "insert");

        return mView;
    }

    @Override
    @RequestMapping(BEAN_PREFIX + "insertDeal")
    protected ModelAndView insertDeal(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mView = getMView("NewsCat", "insert");

        HttpSession session = request.getSession();

        Member loginUser = (Member) session.getAttribute(UIConst.BG_LOGINUSER_KEY);

        String catName = request.getParameter("catName");
        String catDesc = request.getParameter("catDesc");

        request.setAttribute("catName", catName);
        request.setAttribute("catDesc", catDesc);

        String vMsg = "";
        if (SysFun.isNullOrEmpty(catName)) {
            vMsg += "类目名称不能为空。";
        } else if (newsCatService.loadByCatName(catName) != null) {
            vMsg += "类目已存在。";
        }
        if (SysFun.isNullOrEmpty(catDesc)) {
            vMsg += "类目描述不能为空。";
        }

        if (!SysFun.isNullOrEmpty(vMsg)) {
            request.setAttribute("msg", vMsg);
            return mView;
        }

        Date date = new Date();
        Long userId = loginUser.getUserId();

        NewsCat bean = new NewsCat();
        bean.setCatName(catName);
        bean.setCatDesc(catDesc);
        bean.setStatus("");
        bean.setIsDeleted(0L);
        bean.setCreateBy(userId);
        bean.setUpdateBy(userId);
        bean.setCreateOn(date);
        bean.setUpdateOn(date);

        Long result = 0L;

        try {
            result = newsCatService.insert(bean);
        } catch (Exception ex) {
            vMsg = "添加失败。" + ex.getMessage();
        }

        if (result > 0) {
            mView.setViewName(getDispatcherPath("Go", "preload"));
        } else {
            request.setAttribute("msg", vMsg);
        }

        return mView;
    }

    @Override
    @RequestMapping(BEAN_PREFIX + "update")
    protected ModelAndView updateView(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mView = getMView("NewsCat", "update");

        String strCatId = request.getParameter("catId");
        if (!SysFun.isNullOrEmpty(strCatId)) {
            Long catId = Long.valueOf(strCatId);
            NewsCat bean = newsCatService.load(catId);
            if (bean != null) {
                request.setAttribute("catId", catId);
                request.setAttribute("catName", bean.getCatName());
                request.setAttribute("catDesc", bean.getCatDesc());

                return mView;
            }
        }

        mView.setViewName(getDispatcherPath("Go", "preload"));

        return mView;
    }

    @Override
    @RequestMapping(BEAN_PREFIX + "updateDeal")
    protected ModelAndView updateDeal(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mView = getMView("NewsCat", "update");

        HttpSession session = request.getSession();

        Member loginUser = (Member) session.getAttribute(UIConst.BG_LOGINUSER_KEY);

        Long catId = null;
        String strCatId = request.getParameter("catId");
        String catName = request.getParameter("catName");
        String catDesc = request.getParameter("catDesc");

        request.setAttribute("strCatId", strCatId);
        request.setAttribute("catName", catName);
        request.setAttribute("catDesc", catDesc);

        String vMsg = "";
        if (SysFun.isNullOrEmpty(strCatId)) {
            vMsg += "类目ID不能为空。";
        } else {
            catId = Long.valueOf(strCatId);
            if (newsCatService.load(catId) == null) {
                vMsg = "数据不存在。";
            }
        }
        if (SysFun.isNullOrEmpty(catName)) {
            vMsg += "类目名称不能为空。";
        } else {
            NewsCat bean = newsCatService.loadByCatName(catName);
            if (bean != null && !bean.getCatId().equals(catId)) {
                vMsg += "类目已存在。";
            }
        }
        if (SysFun.isNullOrEmpty(catDesc)) {
            vMsg += "类目描述不能为空。";
        }

        if (!SysFun.isNullOrEmpty(vMsg)) {
            request.setAttribute("msg", vMsg);
            return mView;
        }

        Date date = new Date();
        Long userId = loginUser.getUserId();

        NewsCat bean = new NewsCat();
        bean.setCatId(catId);
        bean.setCatName(catName);
        bean.setCatDesc(catDesc);
        bean.setUpdateBy(userId);
        bean.setUpdateOn(date);

        Long result = newsCatService.update(bean);

        if (result > 0) {
            mView.setViewName(getDispatcherPath("Go", "preload"));
        } else {
            request.setAttribute("msg", "修改失败");
        }

        return mView;
    }

    @Override
    @RequestMapping(BEAN_PREFIX + "detail")
    protected ModelAndView detailView(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mView = getMView("NewsCat", "detail");

        Long catId = null;
        String strCatId = request.getParameter("catId");

        if (!SysFun.isNullOrEmpty(strCatId)) {
            catId = Long.valueOf(strCatId);
            NewsCat bean = newsCatService.load(catId);
            if (bean != null) {
                request.setAttribute("bean", bean);

                return mView;
            }
        }

        mView.setViewName(getDispatcherPath("Go", "preload"));

        return mView;
    }

    @Override
    protected ModelAndView detailDeal(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mView = getMView("NewsCat", "detail");



        return mView;
    }

    @Override
    protected ModelAndView deleteView(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mView = getMView("NewsCat", "delete");



        return mView;
    }

    @Override
    @RequestMapping(BEAN_PREFIX + "deleteDeal")
    protected ModelAndView deleteDeal(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mView = getMView("NewsCat", "delete");

        String strCatId = request.getParameter("catId");
        if (!SysFun.isNullOrEmpty(strCatId)) {
            Long catId = Long.valueOf(strCatId);

            Long result = newsCatService.delete(catId);
            if (result > 0) {
                mView.setViewName(getDispatcherPath("Go", "ok"));

                return mView;
            }
        }

        mView.setViewName(getDispatcherPath("Go", "no"));

        return mView;
    }

    @RequestMapping(BEAN_PREFIX + "deleteNotAjax")
    protected ModelAndView deleteNotAjax(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mView = getMView("NewsCat", "delete");


        String strCatId = request.getParameter("catId");
        if (!SysFun.isNullOrEmpty(strCatId)) {
            Long catId = Long.valueOf(strCatId);

            Long result = newsCatService.delete(catId);
            if (result > 0) {
                mView.setViewName(getDispatcherPath("Go", "ok"));

                return mView;
            }
        }

        mView.setViewName(getDispatcherPath("Go", "no"));

        return mView;
    }
}
