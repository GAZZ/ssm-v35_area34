package edu.uc.ui.ctrl34;

import com.liuvei.common.RandFun;
import com.liuvei.common.ValidateCodeFun;
import edu.uc.ui.ctrl02.UIConst;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;

@Controller
@RequestMapping(edu.uc.ui.ctrl34.UIConst.AREAPATH)
public class ValidateCodeController extends BaseController {

    @RequestMapping("/ValidateCode")
    protected ModelAndView ValidateCode(HttpServletRequest request, HttpServletResponse response) throws Exception {

        ModelAndView mView = handleBase(request, response);

        HttpSession session = request.getSession();

        response.setHeader("Pragma", "no-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expirse", 0);
        response.setContentType("image/jpeg");

        String strCode = RandFun.rand4Num().toString();
        session.setAttribute(UIConst.BG_VALIDATE_CODE_KEY, strCode);
        BufferedImage image = ValidateCodeFun.generalImage(strCode);

        ServletOutputStream outStream = response.getOutputStream();
        ImageIO.write(image, "jpeg", outStream);
        outStream.close();
        response.flushBuffer();

        return null;
    }

}
