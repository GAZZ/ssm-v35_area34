package edu.uc.ui.ctrl34;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping(UIConst.AREAPATH)
public class IndexController extends BaseController {

    @RequestMapping("/Index")
    protected ModelAndView index(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        ModelAndView mView = getMView("Index");

        mView.addObject("country", "美国");
        mView.addObject("q", httpServletRequest.getParameter("q"));

        return mView;
    }

}
