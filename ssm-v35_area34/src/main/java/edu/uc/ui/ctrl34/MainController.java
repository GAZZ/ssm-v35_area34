package edu.uc.ui.ctrl34;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
@RequestMapping(UIConst.AREAPATH)
public class MainController extends BaseController {

    @RequestMapping("/Main_welcome")
    private ModelAndView welcomeView(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ModelAndView mView = getMView("Welcome");

        return mView;
    }

    @RequestMapping("/Main")
    private ModelAndView mainView(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ModelAndView mView = getMView("Main");

        return mView;
    }

}
