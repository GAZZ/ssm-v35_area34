package edu.uc.ui.ctrl34;

import com.liuvei.common.SysFun;
import edu.uc.bean.Member;
import edu.uc.service.MemberService;
import edu.uc.ui.ctrl02.UIConst;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.net.URLDecoder;

@Controller
@RequestMapping(edu.uc.ui.ctrl34.UIConst.AREAPATH)
public class LoginController extends BaseController {

    @Resource
    private MemberService memberService;

    @RequestMapping("/Login_loginDeal")
    private ModelAndView loginDeal(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ModelAndView mView = getMView("Login");

        HttpSession session = request.getSession();

        String userName = request.getParameter("userName");
        String userPass = request.getParameter("userPass");
        String validateCode = request.getParameter("validateCode");

        request.setAttribute("userName", userName);

        String msg = "";
        request.setAttribute("msg", msg);
        if (userName == null || userName.isEmpty()) {
            msg = "请填写用户名";
            request.setAttribute("msg", msg);
            return mView;
        }
        if (userPass == null || userPass.isEmpty()) {
            msg = "请填写密码";
            request.setAttribute("msg", msg);
            return mView;
        }
        if (validateCode == null || validateCode.isEmpty()) {
            msg = "请填写验证码";
            request.setAttribute("msg", msg);
            return mView;
        }

        String strCode = (String) session.getAttribute(edu.uc.ui.ctrl02.UIConst.BG_VALIDATE_CODE_KEY);
        if (!validateCode.equals(strCode)) {
            msg = "验证码不正确";
            request.setAttribute("msg", msg);
            return mView;
        }

        Member bean = memberService.loadByUserName(userName);
        if (bean == null) {
            msg = "该用户不存在";
            request.setAttribute("msg", msg);
            return mView;
        }
        if (!bean.getUserPass().equals(userPass)) {
            msg = "密码错误";
            request.setAttribute("msg", msg);
            return mView;
        }

        session.setAttribute(UIConst.BG_LOGINUSER_KEY, bean);

        mView.setViewName(getRedirectPath("Main"));

        String redirectUrl = request.getParameter("redirectUrl");
        if (!SysFun.isNullOrEmpty(redirectUrl)) {
            redirectUrl = URLDecoder.decode(redirectUrl, "UTF-8");
            mView.setViewName("redirect:" + redirectUrl);
        }

        return mView;
    }

    @RequestMapping("/Login_logoutDeal")
    private ModelAndView logoutDeal(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ModelAndView mView = getMView("Login");

        HttpSession session = request.getSession();
        session.invalidate();

        mView.setViewName(getRedirectLogin());
        return mView;
    }

    @RequestMapping("/Login")
    private ModelAndView loginView(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ModelAndView mView = getMView("Login");

        return mView;
    }

}
