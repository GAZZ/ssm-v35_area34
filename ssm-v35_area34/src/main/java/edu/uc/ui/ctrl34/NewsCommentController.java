package edu.uc.ui.ctrl34;

import com.liuvei.common.PagerItem;
import com.liuvei.common.SysFun;
import edu.uc.bean.Member;
import edu.uc.bean.NewsComment;
import edu.uc.service.NewsCommentService;
import edu.uc.service.NewsService;
import edu.uc.ui.ctrl02.UIConst;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping(edu.uc.ui.ctrl34.UIConst.AREAPATH)
public class NewsCommentController extends CrudController {

    @Resource
    private NewsCommentService newsCommentService;

    @Resource
    private NewsService newsService;

    private static final String BEAN_PREFIX = "/NewsComment_";

    @Override
    @RequestMapping(BEAN_PREFIX + "list")
    protected ModelAndView listView(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mView = getMView("NewsComment", "list");

        List<NewsComment> vDataList = null;

        PagerItem pagerItem = new PagerItem();
        pagerItem.parsePageSize(request.getParameter(pagerItem.getParamPageSize()));
        pagerItem.parsePageNum(request.getParameter(pagerItem.getParamPageNum()));

        Long rowCount = 0L;
        rowCount = newsCommentService.count();
        pagerItem.changeRowCount(rowCount);

        vDataList = newsCommentService.pager(pagerItem.getPageNum(), pagerItem.getPageSize());

        pagerItem.changeUrl(SysFun.generalUrl(request.getRequestURI(), request.getQueryString()));

        request.setAttribute("pagerItem", pagerItem);
        request.setAttribute("DataList", vDataList);

        return mView;
    }

    @Override
    @RequestMapping(BEAN_PREFIX + "listDeal")
    protected ModelAndView listDeal(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mView = getMView("NewsComment", "list");

        List<NewsComment> vDataList = null;

        String author = request.getParameter("author");
        Long newsId = null;
        String strNewsId = request.getParameter("newsId");

        request.setAttribute("author", author);

        if (!SysFun.isNullOrEmpty(strNewsId)) {
            newsId = Long.valueOf(strNewsId);
            request.setAttribute("newsId", newsId);
        }

        PagerItem pagerItem = new PagerItem();
        pagerItem.parsePageSize(request.getParameter(pagerItem.getParamPageSize()));
        pagerItem.parsePageNum(request.getParameter(pagerItem.getParamPageNum()));

        Long rowCount = 0L;

        if (SysFun.isNullOrEmpty(author)) {
            if (newsId == null) {
                rowCount = newsCommentService.count();
                pagerItem.changeRowCount(rowCount);
                vDataList = newsCommentService.pager(pagerItem.getPageNum(), pagerItem.getPageSize());
            } else {
                rowCount = newsCommentService.countByNewsId(newsId);
                pagerItem.changeRowCount(rowCount);
                vDataList = newsCommentService.pagerByNewsId(newsId, pagerItem.getPageNum(), pagerItem.getPageSize());
            }
        } else {
            if (newsId == null) {
                rowCount = newsCommentService.countByAuthor(author);
                pagerItem.changeRowCount(rowCount);
                vDataList = newsCommentService.pagerByAuthor(author, pagerItem.getPageNum(), pagerItem.getPageSize());
            } else {
                rowCount = newsCommentService.countByAuthorAndNewsId(author, newsId);
                pagerItem.changeRowCount(rowCount);
                vDataList = newsCommentService.pagerByAuthorAndNewsId(author, newsId, pagerItem.getPageNum(), pagerItem.getPageSize());
            }
        }

        pagerItem.changeUrl(SysFun.generalUrl(request.getRequestURI(), request.getQueryString()));

        request.setAttribute("pagerItem", pagerItem);
        request.setAttribute("DataList", vDataList);

        return mView;
    }

    @Override
    @RequestMapping(BEAN_PREFIX + "insert")
    protected ModelAndView insertView(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mView = getMView("NewsComment", "insert");

        return mView;
    }

    @Override
    @RequestMapping(BEAN_PREFIX + "insertDeal")
    protected ModelAndView insertDeal(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mView = getMView("NewsComment", "insert");

        HttpSession session = request.getSession();

        Member loginUser = (Member) session.getAttribute(UIConst.BG_LOGINUSER_KEY);

        Long newsId = null;
        String strNewsId = request.getParameter("newsId");
        String content = request.getParameter("content");
        String author = request.getParameter("author");

        request.setAttribute("newsId", newsId);
        request.setAttribute("content", content);
        request.setAttribute("author", author);

        String vMsg = "";
        if (SysFun.isNullOrEmpty(strNewsId)) {
            vMsg += "新闻ID不能为空。";
        } else {
            newsId = Long.valueOf(strNewsId);
            if (newsService.load(newsId) == null) {
                vMsg += "新闻ID不存在。";
            }
        }
        if (SysFun.isNullOrEmpty(content)) {
            vMsg += "内容不能为空。";
        }
        if (SysFun.isNullOrEmpty(author)) {
            vMsg += "作者不能为空。";
        }

        if (!SysFun.isNullOrEmpty(vMsg)) {
            request.setAttribute("msg", vMsg);
            return mView;
        }

        Date date = new Date();
        Long userId = loginUser.getUserId();

        NewsComment bean = new NewsComment();
        bean.setNewsId(newsId);
        bean.setContent(content);
        bean.setAuthor(author);
        bean.setStatus("");
        bean.setIsDeleted(0L);
        bean.setCreateBy(userId);
        bean.setUpdateBy(userId);
        bean.setCreateOn(date);
        bean.setUpdateOn(date);

        Long result = 0L;

        try {
            result = newsCommentService.insert(bean);
        } catch (Exception ex) {
            vMsg = "添加失败。" + ex.getMessage();
        }

        if (result > 0) {
            mView.setViewName(getDispatcherPath("Go", "preload"));
        } else {
            request.setAttribute("msg", vMsg);
        }

        return mView;
    }

    @Override
    @RequestMapping(BEAN_PREFIX + "update")
    protected ModelAndView updateView(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mView = getMView("NewsComment", "update");

        String strId = request.getParameter("id");
        if (!SysFun.isNullOrEmpty(strId)) {
            Long id = Long.valueOf(strId);
            NewsComment bean = newsCommentService.load(id);
            if (bean != null) {
                request.setAttribute("id", id);
                request.setAttribute("newsId", bean.getNewsId());
                request.setAttribute("content", bean.getContent());
                request.setAttribute("author", bean.getAuthor());

                return mView;
            }
        }

        mView.setViewName(getDispatcherPath("Go", "preload"));

        return mView;
    }

    @Override
    @RequestMapping(BEAN_PREFIX + "updateDeal")
    protected ModelAndView updateDeal(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mView = getMView("NewsComment", "update");

        HttpSession session = request.getSession();

        Member loginUser = (Member) session.getAttribute(UIConst.BG_LOGINUSER_KEY);

        Long id = null;
        String strId = request.getParameter("id");
        Long newsId = null;
        String strNewsId = request.getParameter("newsId");
        String content = request.getParameter("content");
        String author = request.getParameter("author");

        request.setAttribute("id", strId);
        request.setAttribute("newsId", newsId);
        request.setAttribute("content", content);
        request.setAttribute("author", author);

        String vMsg = "";
        if (SysFun.isNullOrEmpty(strId)) {
            vMsg += "ID不能为空。";
        } else {
            id = Long.valueOf(strId);
            if (newsCommentService.load(id) == null) {
                vMsg += "数据不存在。";
            }
        }
        if (SysFun.isNullOrEmpty(strNewsId)) {
            vMsg += "新闻ID不能为空。";
        } else {
            newsId = Long.valueOf(strNewsId);
            if (newsService.load(newsId) == null) {
                vMsg += "新闻ID不存在。";
            }
        }
        if (SysFun.isNullOrEmpty(content)) {
            vMsg += "内容不能为空。";
        }
        if (SysFun.isNullOrEmpty(author)) {
            vMsg += "作者不能为空。";
        }

        if (!SysFun.isNullOrEmpty(vMsg)) {
            request.setAttribute("msg", vMsg);
            return mView;
        }

        Date date = new Date();
        Long userId = loginUser.getUserId();

        NewsComment bean = new NewsComment();
        bean.setId(id);
        bean.setNewsId(newsId);
        bean.setContent(content);
        bean.setAuthor(author);
        bean.setUpdateBy(userId);
        bean.setUpdateOn(date);

        Long result = newsCommentService.update(bean);

        if (result > 0) {
            mView.setViewName(getDispatcherPath("Go", "preload"));
        } else {
            request.setAttribute("msg", "修改失败");
        }

        return mView;
    }

    @Override
    @RequestMapping(BEAN_PREFIX + "detail")
    protected ModelAndView detailView(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mView = getMView("NewsComment", "detail");

        Long id = null;
        String strId = request.getParameter("id");

        if (!SysFun.isNullOrEmpty(strId)) {
            id = Long.valueOf(strId);
            NewsComment bean = newsCommentService.load(id);
            if (bean != null) {
                request.setAttribute("bean", bean);

                return mView;
            }
        }

        mView.setViewName(getDispatcherPath("Go", "preload"));

        return mView;
    }

    @Override
    protected ModelAndView detailDeal(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mView = getMView("NewsComment", "detail");



        return mView;
    }

    @Override
    protected ModelAndView deleteView(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mView = getMView("NewsComment", "delete");



        return mView;
    }

    @Override
    @RequestMapping(BEAN_PREFIX + "deleteDeal")
    protected ModelAndView deleteDeal(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mView = getMView("NewsComment", "delete");

        String strId = request.getParameter("id");
        if (!SysFun.isNullOrEmpty(strId)) {
            Long id = Long.valueOf(strId);

            Long result = newsCommentService.delete(id);
            if (result > 0) {
                mView.setViewName(getDispatcherPath("Go", "ok"));

                return mView;
            }
        }

        mView.setViewName(getDispatcherPath("Go", "no"));

        return mView;
    }

    @RequestMapping(BEAN_PREFIX + "deleteNotAjax")
    protected ModelAndView deleteNotAjax(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mView = getMView("NewsComment", "delete");


        String strId = request.getParameter("id");
        if (!SysFun.isNullOrEmpty(strId)) {
            Long id = Long.valueOf(strId);

            Long result = newsCommentService.delete(id);
            if (result > 0) {
                mView.setViewName(getDispatcherPath("Go", "ok"));

                return mView;
            }
        }

        mView.setViewName(getDispatcherPath("Go", "no"));

        return mView;
    }
}
