package edu.uc.ui.ctrl34;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping(UIConst.AREAPATH)
public class DemoController {

    @RequestMapping("/Demo")
    protected ModelAndView demo(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        ModelAndView mView = new ModelAndView();

        mView.addObject("country", "中华人民共和国");

        mView.setViewName("view34/Demo");

        return mView;
    }

}
