package edu.uc.ui.ctrl34;

/**
 * UI常量
 * 
 * @author GAZZ
 *
 */
public class UIConst {

	/**
	 * URL子路径
	 */
	public final static String AREANAME = "area34";

	/**
	 * 页面路径
	 */
	public final static String VIEWNAME = "view34";

	/**
	 * URL全路径
	 */
	public final static String AREAPATH = "/" + AREANAME;

	/**
	 * 页面根
	 */
	public final static String VIEWROOT = "/WEB-INF";

	/**
	 * 页面全路径
	 */
	public final static String VIEWPATH = VIEWROOT + "/" + VIEWNAME;

	/**
	 * 前台登录账号的会话key：会员
	 */
	public static final String BG_LOGINUSER_KEY = "BG_LOGINUSER_KEY";

	/**
	 * 其他登录账号的会话key
	 */
	public static final String FG_LOGINUSER_KEY = "FG_LOGINUSER_KEY";

	/**
	 * 后台登录账号是否管理员的会话key
	 */
	public static final String OTHER_LOGINUSER_KEY = "OTHER_LOGINUSER_KEY";

	/**
	 * 后台验证码的会话key
	 */
	public static final String BG_ISADMIN_KEY = "BG_ISADMIN_KEY";

	/**
	 * URL子路径
	 */
	public static final String BG_VALIDATE_CODE_KEY = "BG_VALIDATE_CODE_KEY";

    public static final String UP_ROOT = "up";
}
