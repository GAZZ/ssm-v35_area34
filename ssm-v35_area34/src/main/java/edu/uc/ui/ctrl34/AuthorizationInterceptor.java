package edu.uc.ui.ctrl34;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.net.URLEncoder;

public class AuthorizationInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String contextPath = request.getContextPath();
        String currUrl = URLEncoder.encode(request.getServletPath(), "UTF-8");
        HttpSession session = request.getSession();

        System.out.println("AuthorizationInterceptor begin ......");

        if (session.getAttribute(UIConst.BG_LOGINUSER_KEY) == null) {
            String loginUrl = contextPath + "/" + UIConst.AREANAME + "/Login";
            loginUrl += "?redirectUrl=" + currUrl;
            response.sendRedirect(loginUrl);
            System.out.println(false);
            return false;
        }

        System.out.println("AuthorizationInterceptor finished ......");
        System.out.println(true);
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
