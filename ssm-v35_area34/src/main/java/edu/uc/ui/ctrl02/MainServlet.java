package edu.uc.ui.ctrl02;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(UIConst.AREAPATH + "/Main")
public class MainServlet extends BaseServlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// 设置请求对象的编码方式
		request.setCharacterEncoding("utf-8");
		// 设置相应对象的编码方式
		response.setCharacterEncoding("utf-8");
		// 设置相应的内容类型为text/html
		response.setContentType("text/html; charset=utf-8");
		
		if (!checkLogin(request, response)) {
			return;
		}
		
		String oper = request.getParameter("oper");
		
		if (oper != null && oper.equalsIgnoreCase("welcome")) {
			welcomeView(request, response);
		} else {
			mainView(request, response);
		}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	private void welcomeView(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		String toPage = UIConst.VIEWPATH + "/Welcome.jsp";
//		request.getRequestDispatcher(toPage).forward(request, response);
		String toPage = getPagePath("Welcome");
		forward(toPage, request, response);
	}

	private void mainView(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		String toPage = UIConst.VIEWPATH + "/Main.jsp";
//		request.getRequestDispatcher(toPage).forward(request, response);
		String toPage = getPagePath("Main");
		forward(toPage, request, response);
	}

}
