package edu.uc.ui.ctrl02;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public abstract class CrudServlet extends BaseServlet {

	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		currAreaPath(request);
		doCrud(request, response);
	}

	protected void doCrud(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		response.setContentType("text/html; charset=utf-8");

		if (!checkLogin(request, response)) {
			return;
		}

		String oper = request.getParameter("oper");
		if (oper == null) {
			oper = "";
		} else {
			oper = oper.trim().toLowerCase();
		}

		switch (oper) {
		case "list":
			listView(request, response);
			break;
		case "listdeal":
			listDeal(request, response);
			break;
		case "insert":
			insertView(request, response);
			break;
		case "insertdeal":
			insertDeal(request, response);
			break;
		case "update":
			updateView(request, response);
			break;
		case "updatedeal":
			updateDeal(request, response);
			break;
		case "deletedeal":
			deleteDeal(request, response);
			break;
		case "deletenotajax":
			deleteNotAjax(request, response);
			break;
		case "detail":
			detailView(request, response);
			break;
		default:
			listView(request, response);
			break;
		}
	}

	protected abstract void listView(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;

	protected abstract void listDeal(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;

	protected abstract void insertView(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;

	protected abstract void insertDeal(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;

	protected abstract void updateView(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;

	protected abstract void updateDeal(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;

	protected abstract void deleteDeal(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;

	protected abstract void deleteNotAjax(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;

	protected abstract void detailView(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;

}
