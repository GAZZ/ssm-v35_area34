package edu.uc.ui.ctrl02;

import com.liuvei.common.PagerItem;
import com.liuvei.common.SysFun;
import edu.uc.bean.Member;
import edu.uc.bean.NewsComment;
import edu.uc.service.NewsCommentService;
import edu.uc.service.NewsService;
import edu.util.SpringUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;

@WebServlet(UIConst.AREAPATH + "/NewsComment")
public class NewsCommentServlet extends CrudServlet {

	private static final long serialVersionUID = 1L;

	private NewsCommentService newsCommentService = SpringUtil.getBean("newsCommentService", NewsCommentService.class);
	private NewsService newsService = SpringUtil.getBean("newsService", NewsService.class);

	@Override
	protected void listView(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		List<NewsComment> vDataList = null;

		PagerItem pagerItem = new PagerItem();
		pagerItem.parsePageSize(request.getParameter(pagerItem.getParamPageSize()));
		pagerItem.parsePageNum(request.getParameter(pagerItem.getParamPageNum()));

		Long rowCount = 0L;
		rowCount = newsCommentService.count();
		pagerItem.changeRowCount(rowCount);

		vDataList = newsCommentService.pager(pagerItem.getPageNum(), pagerItem.getPageSize());

		pagerItem.changeUrl(SysFun.generalUrl(request.getRequestURI(), request.getQueryString()));

		request.setAttribute("pagerItem", pagerItem);
		request.setAttribute("DataList", vDataList);

//		String toPage = UIConst.VIEWPATH + "/NewsComment_list.jsp";
//		request.getRequestDispatcher(toPage).forward(request, response);
		String toPage = getPagePath("NewsComment", "list");
		forward(toPage, request, response);

	}

	@Override
	protected void listDeal(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		List<NewsComment> vDataList = null;

		String author = request.getParameter("author");
		Long newsId = null;
		String strNewsId = request.getParameter("newsId");
		
		request.setAttribute("author", author);

		if (!SysFun.isNullOrEmpty(strNewsId)) {
			newsId = Long.valueOf(strNewsId);
			request.setAttribute("newsId", newsId);
		}

		PagerItem pagerItem = new PagerItem();
		pagerItem.parsePageSize(request.getParameter(pagerItem.getParamPageSize()));
		pagerItem.parsePageNum(request.getParameter(pagerItem.getParamPageNum()));

		Long rowCount = 0L;

		if (SysFun.isNullOrEmpty(author)) {
			if (newsId == null) {
				rowCount = newsCommentService.count();
				pagerItem.changeRowCount(rowCount);
				vDataList = newsCommentService.pager(pagerItem.getPageNum(), pagerItem.getPageSize());
			} else {
				rowCount = newsCommentService.countByNewsId(newsId);
				pagerItem.changeRowCount(rowCount);
				vDataList = newsCommentService.pagerByNewsId(newsId, pagerItem.getPageNum(), pagerItem.getPageSize());
			}
		} else {
			if (newsId == null) {
				rowCount = newsCommentService.countByAuthor(author);
				pagerItem.changeRowCount(rowCount);
				vDataList = newsCommentService.pagerByAuthor(author, pagerItem.getPageNum(), pagerItem.getPageSize());
			} else {
				rowCount = newsCommentService.countByAuthorAndNewsId(author, newsId);
				pagerItem.changeRowCount(rowCount);
				vDataList = newsCommentService.pagerByAuthorAndNewsId(author, newsId, pagerItem.getPageNum(), pagerItem.getPageSize());
			}
		}

		pagerItem.changeUrl(SysFun.generalUrl(request.getRequestURI(), request.getQueryString()));

		request.setAttribute("pagerItem", pagerItem);
		request.setAttribute("DataList", vDataList);

//		String toPage = UIConst.VIEWPATH + "/NewsComment_list.jsp";
//		request.getRequestDispatcher(toPage).forward(request, response);
		String toPage = getPagePath("NewsComment", "list");
		forward(toPage, request, response);

	}

	@Override
	protected void insertView(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		String toPage = UIConst.VIEWPATH + "/NewsComment_insert.jsp";
//		request.getRequestDispatcher(toPage).forward(request, response);
		String toPage = getPagePath("NewsComment", "insert");
		forward(toPage, request, response);
	}

	@Override
	protected void insertDeal(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		PrintWriter out = response.getWriter();
		HttpSession session = request.getSession();

		Member loginUser = (Member) session.getAttribute(UIConst.BG_LOGINUSER_KEY);

		Long newsId = null;
		String strNewsId = request.getParameter("newsId");
		String content = request.getParameter("content");
		String author = request.getParameter("author");

		request.setAttribute("newsId", newsId);
		request.setAttribute("content", content);
		request.setAttribute("author", author);

		String vMsg = "";
		if (SysFun.isNullOrEmpty(strNewsId)) {
			vMsg += "新闻ID不能为空。";
		} else {
			newsId = Long.valueOf(strNewsId);
			if (newsService.load(newsId) == null) {
				vMsg += "新闻ID不存在。";
			}
		}
		if (SysFun.isNullOrEmpty(content)) {
			vMsg += "内容不能为空。";
		}
		if (SysFun.isNullOrEmpty(author)) {
			vMsg += "作者不能为空。";
		}

		if (!SysFun.isNullOrEmpty(vMsg)) {
			request.setAttribute("msg", vMsg);
			insertView(request, response);
			return;
		}

		Date date = new Date();
		Long userId = loginUser.getUserId();

		NewsComment bean = new NewsComment();
		bean.setNewsId(newsId);
		bean.setContent(content);
		bean.setAuthor(author);
		bean.setStatus("");
		bean.setIsDeleted(0L);
		bean.setCreateBy(userId);
		bean.setUpdateBy(userId);
		bean.setCreateOn(date);
		bean.setUpdateOn(date);

		Long result = 0L;

		try {
			result = newsCommentService.insert(bean);
		} catch (Exception ex) {
			vMsg = "添加失败。" + ex.getMessage();
		}

		if (result > 0) {
			out.println("<script>");
			out.println("parent.window.location.reload();");
			out.println("</script>");
		} else {
			request.setAttribute("msg", vMsg);
			insertView(request, response);
		}

	}

	@Override
	protected void updateView(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		String strId = request.getParameter("id");
		if (!SysFun.isNullOrEmpty(strId)) {
			Long id = Long.valueOf(strId);
			NewsComment bean = newsCommentService.load(id);
			if (bean != null) {
				request.setAttribute("id", id);
				request.setAttribute("newsId", bean.getNewsId());
				request.setAttribute("content", bean.getContent());
				request.setAttribute("author", bean.getAuthor());
//				String toPage = UIConst.VIEWPATH + "/NewsComment_update.jsp";
//				request.getRequestDispatcher(toPage).forward(request, response);
				String toPage = getPagePath("NewsComment", "update");
				forward(toPage, request, response);
				return;
			}
		}
		
		out.print("<script>");
		out.print("alert('数据不存在。');");
		out.print("parent.window.location.reload();");
		out.print("</script>");
		
	}

	@Override
	protected void updateDeal(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		PrintWriter out = response.getWriter();
		HttpSession session = request.getSession();

		Member loginUser = (Member) session.getAttribute(UIConst.BG_LOGINUSER_KEY);

		Long id = null;
		String strId = request.getParameter("id");
		Long newsId = null;
		String strNewsId = request.getParameter("newsId");
		String content = request.getParameter("content");
		String author = request.getParameter("author");

		request.setAttribute("id", strId);
		request.setAttribute("newsId", newsId);
		request.setAttribute("content", content);
		request.setAttribute("author", author);

		String vMsg = "";
		if (SysFun.isNullOrEmpty(strId)) {
			vMsg += "ID不能为空。";
		} else {
			id = Long.valueOf(strId);
			if (newsCommentService.load(id) == null) {
				vMsg += "数据不存在。";
			}
		}
		if (SysFun.isNullOrEmpty(strNewsId)) {
			vMsg += "新闻ID不能为空。";
		} else {
			newsId = Long.valueOf(strNewsId);
			if (newsService.load(newsId) == null) {
				vMsg += "新闻ID不存在。";
			}
		}
		if (SysFun.isNullOrEmpty(content)) {
			vMsg += "内容不能为空。";
		}
		if (SysFun.isNullOrEmpty(author)) {
			vMsg += "作者不能为空。";
		}

		if (!SysFun.isNullOrEmpty(vMsg)) {
			request.setAttribute("msg", vMsg);
			insertView(request, response);
			return;
		}

		Date date = new Date();
		Long userId = loginUser.getUserId();

		NewsComment bean = new NewsComment();
		bean.setId(id);
		bean.setNewsId(newsId);
		bean.setContent(content);
		bean.setAuthor(author);
		bean.setUpdateBy(userId);
		bean.setUpdateOn(date);

		Long result = newsCommentService.update(bean);

		if (result > 0) {
			out.println("<script>");
			out.println("parent.window.location.reload();");
			out.println("</script>");
		} else {
			request.setAttribute("msg", "修改失败");
			insertView(request, response);
		}
		
	}

	@Override
	protected void deleteDeal(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();

		String strId = request.getParameter("id");
		if (!SysFun.isNullOrEmpty(strId)) {
			Long id = Long.valueOf(strId);

			Long result = newsCommentService.delete(id);
			if (result > 0) {
				out.print("ok");
				return;
			}
		}

		out.print("nook");
	}

	@Override
	protected void deleteNotAjax(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		PrintWriter out = response.getWriter();

		String strId = request.getParameter("id");
		if (!SysFun.isNullOrEmpty(strId)) {
			Long id = Long.valueOf(strId);

			Long result = newsCommentService.delete(id);
			if (result > 0) {
				out.print("<script>");
				out.print("alert('删除成功');");
				out.print("location.href='NewsComment?oper=list';");
				out.print("</script>");
				return;
			}
		}

		out.print("<script>");
		out.print("alert('删除失败');");
		out.print("location.href='NewsComment?oper=list';");
		out.print("</script>");

	}

	@Override
	protected void detailView(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		
		Long id = null;
		String strId = request.getParameter("id");
		
		if (!SysFun.isNullOrEmpty(strId)) {
			id = Long.valueOf(strId);
			NewsComment bean = newsCommentService.load(id);
			if (bean != null) {
				request.setAttribute("bean", bean);
				
//				String toPage = UIConst.VIEWPATH + "/NewsComment_detail.jsp";
//				request.getRequestDispatcher(toPage).forward(request, response);
				String toPage = getPagePath("NewsComment", "detail");
				forward(toPage, request, response);
				return;
			}
		}
		
		out.print("<script>");
		out.print("alert('数据不存在。');");
		out.print("parent.window.location.reload();");
		out.print("</script>");
		
	}

}
