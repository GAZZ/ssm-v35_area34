package edu.uc.ui.ctrl02;

import edu.uc.bean.Member;
import edu.uc.service.MemberService;
import edu.util.SpringUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(UIConst.AREAPATH + "/Login")
public class LoginServlet extends BaseServlet {

	private static final long serialVersionUID = 1L;

	private MemberService memberService = SpringUtil.getBean("memberService", MemberService.class);

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// 设置请求对象的编码方式
		request.setCharacterEncoding("utf-8");
		// 设置相应对象的编码方式
		response.setCharacterEncoding("utf-8");
		// 设置相应的内容类型为text/html
		response.setContentType("text/html; charset=utf-8");

		String oper = request.getParameter("oper");

		if (oper != null && oper.equalsIgnoreCase("loginDeal")) {
			loginDeal(request, response);
		} else if (oper != null && oper.equalsIgnoreCase("logoutDeal")) {
			logoutDeal(request, response);
		} else {
			loginView(request, response);
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	private void loginDeal(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
//		ServletContext application = request.getServletContext();

		String userName = request.getParameter("userName");
		String userPass = request.getParameter("userPass");
		String validateCode = request.getParameter("validateCode");

		request.setAttribute("userName", userName);

		//String toPage = UIConst.VIEWPATH + "/Login.jsp";
		String toPage = getPagePath("Login");
		String msg = "";
		request.setAttribute("msg", msg);
		if (userName == null || userName.isEmpty()) {
			msg = "请填写用户名";
			request.setAttribute("msg", msg);
			//request.getRequestDispatcher(toPage).forward(request, response);
			forward(toPage, request, response);
			return;
		}
		if (userPass == null || userPass.isEmpty()) {
			msg = "请填写密码";
			request.setAttribute("msg", msg);
			//request.getRequestDispatcher(toPage).forward(request, response);
			forward(toPage, request, response);
			return;
		}
		if (validateCode == null || validateCode.isEmpty()) {
			msg = "请填写验证码";
			request.setAttribute("msg", msg);
			//request.getRequestDispatcher(toPage).forward(request, response);
			forward(toPage, request, response);
			return;
		}

		String strCode = (String) session.getAttribute(UIConst.BG_VALIDATE_CODE_KEY);
		if (!validateCode.equals(strCode)) {
			msg = "验证码不正确";
			request.setAttribute("msg", msg);
			//request.getRequestDispatcher(toPage).forward(request, response);
			forward(toPage, request, response);
			return;
		}

		Member bean = memberService.loadByUserName(userName);
		if (bean == null) {
			msg = "该用户不存在";
			request.setAttribute("msg", msg);
			//request.getRequestDispatcher(toPage).forward(request, response);
			forward(toPage, request, response);
			return;
		}
		if (!bean.getUserPass().equals(userPass)) {
			msg = "密码错误";
			request.setAttribute("msg", msg);
			//request.getRequestDispatcher(toPage).forward(request, response);
			forward(toPage, request, response);
			return;
		}

		session.setAttribute(UIConst.BG_LOGINUSER_KEY, bean);

//		String toURL = application.getContextPath() + UIConst.AREAPATH + "/Main";
//		response.sendRedirect(toURL);
		String toURL = getUrlPath("Main");
		redirect(toURL, request, response);

	}

	private void logoutDeal(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		
		//session.removeAttribute(UIConst.BG_LOGINUSER_KEY);
		session.invalidate();
//		String toURL = request.getContextPath() + UIConst.AREAPATH + "/Login";
//		response.sendRedirect(toURL);
		String toURL = getUrlPath("Login");
		redirectAreaPath(toURL, request, response);
	}

	private void loginView(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		String toPage = UIConst.VIEWPATH + "/Login.jsp";
//		request.getRequestDispatcher(toPage).forward(request, response);
		String toPage = getPagePath("Login");
		forward(toPage, request, response);
	}

}
