package edu.uc.ui.ctrl02;

import com.liuvei.common.PagerItem;
import com.liuvei.common.SysFun;
import edu.uc.bean.Member;
import edu.uc.bean.NewsCat;
import edu.uc.service.NewsCatService;
import edu.util.SpringUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;

@WebServlet(UIConst.AREAPATH + "/NewsCat")
public class NewsCatServlet extends CrudServlet {

	private static final long serialVersionUID = 1L;

	private NewsCatService newsCatService = SpringUtil.getBean("newsCatService", NewsCatService.class);
	
	@Override
	protected void listView(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		List<NewsCat> vDataList = null;

		PagerItem pagerItem = new PagerItem();
		pagerItem.parsePageSize(request.getParameter(pagerItem.getParamPageSize()));
		pagerItem.parsePageNum(request.getParameter(pagerItem.getParamPageNum()));

		Long rowCount = 0L;
		rowCount = newsCatService.count();
		pagerItem.changeRowCount(rowCount);

		vDataList = newsCatService.pager(pagerItem.getPageNum(), pagerItem.getPageSize());

		pagerItem.changeUrl(SysFun.generalUrl(request.getRequestURI(), request.getQueryString()));

		request.setAttribute("pagerItem", pagerItem);
		request.setAttribute("DataList", vDataList);

//		String toPage = UIConst.VIEWPATH + "/NewsCat_list.jsp";
//		request.getRequestDispatcher(toPage).forward(request, response);
		String toPage = getPagePath("NewsCat", "list");
		forward(toPage, request, response);

	}

	@Override
	protected void listDeal(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		List<NewsCat> vDataList = null;

		String catName = request.getParameter("catName");
		request.setAttribute("catName", catName);

		PagerItem pagerItem = new PagerItem();
		pagerItem.parsePageSize(request.getParameter(pagerItem.getParamPageSize()));
		pagerItem.parsePageNum(request.getParameter(pagerItem.getParamPageNum()));

		Long rowCount = 0L;

		if (SysFun.isNullOrEmpty(catName)) {
			rowCount = newsCatService.count();
			pagerItem.changeRowCount(rowCount);
			vDataList = newsCatService.pager(pagerItem.getPageNum(), pagerItem.getPageSize());

		} else {
			rowCount = newsCatService.countByCatName(catName);
			pagerItem.changeRowCount(rowCount);
			vDataList = newsCatService.pagerByCatName(catName, pagerItem.getPageNum(), pagerItem.getPageSize());
		}

		pagerItem.changeUrl(SysFun.generalUrl(request.getRequestURI(), request.getQueryString()));

		request.setAttribute("pagerItem", pagerItem);
		request.setAttribute("DataList", vDataList);

//		String toPage = UIConst.VIEWPATH + "/NewsCat_list.jsp";
//		request.getRequestDispatcher(toPage).forward(request, response);
		String toPage = getPagePath("NewsCat", "list");
		forward(toPage, request, response);

	}

	@Override
	protected void insertView(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		String toPage = UIConst.VIEWPATH + "/NewsCat_insert.jsp";
//		request.getRequestDispatcher(toPage).forward(request, response);
		String toPage = getPagePath("NewsCat", "insert");
		forward(toPage, request, response);
	}

	@Override
	protected void insertDeal(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		PrintWriter out = response.getWriter();
		HttpSession session = request.getSession();

		Member loginUser = (Member) session.getAttribute(UIConst.BG_LOGINUSER_KEY);

		String catName = request.getParameter("catName");
		String catDesc = request.getParameter("catDesc");

		request.setAttribute("catName", catName);
		request.setAttribute("catDesc", catDesc);

		String vMsg = "";
		if (SysFun.isNullOrEmpty(catName)) {
			vMsg += "类目名称不能为空。";
		} else if (newsCatService.loadByCatName(catName) != null) {
			vMsg += "类目已存在。";
		}
		if (SysFun.isNullOrEmpty(catDesc)) {
			vMsg += "类目描述不能为空。";
		}

		if (!SysFun.isNullOrEmpty(vMsg)) {
			request.setAttribute("msg", vMsg);
			insertView(request, response);
			return;
		}

		Date date = new Date();
		Long userId = loginUser.getUserId();

		NewsCat bean = new NewsCat();
		bean.setCatName(catName);
		bean.setCatDesc(catDesc);
		bean.setStatus("");
		bean.setIsDeleted(0L);
		bean.setCreateBy(userId);
		bean.setUpdateBy(userId);
		bean.setCreateOn(date);
		bean.setUpdateOn(date);

		Long result = 0L;

		try {
			result = newsCatService.insert(bean);
		} catch (Exception ex) {
			vMsg = "添加失败。" + ex.getMessage();
		}

		if (result > 0) {
			out.println("<script>");
			out.println("parent.window.location.reload();");
			out.println("</script>");
		} else {
			request.setAttribute("msg", vMsg);
			insertView(request, response);
		}

	}

	@Override
	protected void updateView(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		String strCatId = request.getParameter("catId");
		if (!SysFun.isNullOrEmpty(strCatId)) {
			Long catId = Long.valueOf(strCatId);
			NewsCat bean = newsCatService.load(catId);
			if (bean != null) {
				request.setAttribute("catId", catId);
				request.setAttribute("catName", bean.getCatName());
				request.setAttribute("catDesc", bean.getCatDesc());
//				String toPage = UIConst.VIEWPATH + "/NewsCat_update.jsp";
//				request.getRequestDispatcher(toPage).forward(request, response);
				String toPage = getPagePath("NewsCat", "update");
				forward(toPage, request, response);
				return;
			}
		}
		
		out.print("<script>");
		out.print("alert('数据不存在。');");
		out.print("parent.window.location.reload();");
		out.print("</script>");
		
	}

	@Override
	protected void updateDeal(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		HttpSession session = request.getSession();

		Member loginUser = (Member) session.getAttribute(UIConst.BG_LOGINUSER_KEY);

		Long catId = null;
		String strCatId = request.getParameter("catId");
		String catName = request.getParameter("catName");
		String catDesc = request.getParameter("catDesc");

		request.setAttribute("strCatId", strCatId);
		request.setAttribute("catName", catName);
		request.setAttribute("catDesc", catDesc);

		String vMsg = "";
		if (SysFun.isNullOrEmpty(strCatId)) {
			vMsg += "类目ID不能为空。";
		} else {
			catId = Long.valueOf(strCatId);
			if (newsCatService.load(catId) == null) {
				vMsg = "数据不存在。";
			}
		}
		if (SysFun.isNullOrEmpty(catName)) {
			vMsg += "类目名称不能为空。";
		} else {
			NewsCat bean = newsCatService.loadByCatName(catName);
			if (bean != null && !bean.getCatId().equals(catId)) {
				vMsg += "类目已存在。";
			}
		}
		if (SysFun.isNullOrEmpty(catDesc)) {
			vMsg += "类目描述不能为空。";
		}

		if (!SysFun.isNullOrEmpty(vMsg)) {
			request.setAttribute("msg", vMsg);
			insertView(request, response);
			return;
		}

		Date date = new Date();
		Long userId = loginUser.getUserId();

		NewsCat bean = new NewsCat();
		bean.setCatId(catId);
		bean.setCatName(catName);
		bean.setCatDesc(catDesc);
		bean.setUpdateBy(userId);
		bean.setUpdateOn(date);

		Long result = newsCatService.update(bean);

		if (result > 0) {
			out.println("<script>");
			out.println("parent.window.location.reload();");
			out.println("</script>");
		} else {
			request.setAttribute("msg", "修改失败");
			insertView(request, response);
		}
		
	}

	@Override
	protected void deleteDeal(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();

		String strCatId = request.getParameter("catId");
		if (!SysFun.isNullOrEmpty(strCatId)) {
			Long catId = Long.valueOf(strCatId);

			Long result = newsCatService.delete(catId);
			if (result > 0) {
				out.print("ok");
				return;
			}
		}

		out.print("nook");
	}

	@Override
	protected void deleteNotAjax(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		PrintWriter out = response.getWriter();

		String strCatId = request.getParameter("catId");
		if (!SysFun.isNullOrEmpty(strCatId)) {
			Long catId = Long.valueOf(strCatId);

			Long result = newsCatService.delete(catId);
			if (result > 0) {
				out.print("<script>");
				out.print("alert('删除成功');");
				out.print("location.href='NewsCat?oper=list';");
				out.print("</script>");
				return;
			}
		}

		out.print("<script>");
		out.print("alert('删除失败');");
		out.print("location.href='NewsCat?oper=list';");
		out.print("</script>");

	}

	@Override
	protected void detailView(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

PrintWriter out = response.getWriter();
		
		Long catId = null;
		String strCatId = request.getParameter("catId");
		
		if (!SysFun.isNullOrEmpty(strCatId)) {
			catId = Long.valueOf(strCatId);
			NewsCat bean = newsCatService.load(catId);
			if (bean != null) {
				request.setAttribute("bean", bean);
				
//				String toPage = UIConst.VIEWPATH + "/NewsCat_detail.jsp";
//				request.getRequestDispatcher(toPage).forward(request, response);
				String toPage = getPagePath("NewsCat", "detail");
				forward(toPage, request, response);
				return;
			}
		}
		
		out.print("<script>");
		out.print("alert('数据不存在。');");
		out.print("parent.window.location.reload();");
		out.print("</script>");
		
	}

}
