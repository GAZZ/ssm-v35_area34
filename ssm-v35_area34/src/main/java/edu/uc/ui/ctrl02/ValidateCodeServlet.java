package edu.uc.ui.ctrl02;

import com.liuvei.common.RandFun;
import com.liuvei.common.ValidateCodeFun;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;
import java.io.IOException;

@WebServlet(UIConst.AREAPATH + "/ValidateCode")
public class ValidateCodeServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setDateHeader("Expirse", 0);
		response.setContentType("image/jpeg");
		
		String strCode = RandFun.rand4Num().toString();
		session.setAttribute(UIConst.BG_VALIDATE_CODE_KEY, strCode);
		BufferedImage image = ValidateCodeFun.generalImage(strCode);
		
		ServletOutputStream outStream = response.getOutputStream();
		ImageIO.write(image, "jpeg", outStream);
		outStream.close();
		response.flushBuffer();
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
