package edu.uc.ui.ctrl02;

import com.liuvei.common.PagerItem;
import com.liuvei.common.SysFun;
import edu.uc.bean.Member;
import edu.uc.service.MemberService;
import edu.util.SpringUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

@WebServlet(UIConst.AREAPATH + "/Member")
public class MemberServlet extends CrudServlet {

	private static final long serialVersionUID = 1L;

	private MemberService memberService = SpringUtil.getBean("memberService", MemberService.class);

	@Override
	protected void listView(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		List<Member> vDataList = null;

		PagerItem pagerItem = new PagerItem();
		pagerItem.parsePageSize(request.getParameter(pagerItem.getParamPageSize()));
		pagerItem.parsePageNum(request.getParameter(pagerItem.getParamPageNum()));

		Long rowCount = 0L;
		rowCount = memberService.count();
		pagerItem.changeRowCount(rowCount);

		vDataList = memberService.pager(pagerItem.getPageNum(), pagerItem.getPageSize());

		pagerItem.changeUrl(SysFun.generalUrl(request.getRequestURI(), request.getQueryString()));

		request.setAttribute("pagerItem", pagerItem);
		request.setAttribute("DataList", vDataList);

//		String toPage = UIConst.VIEWPATH + "/Member_list.jsp";
//		request.getRequestDispatcher(toPage).forward(request, response);
		String toPage = getPagePath("Member", "list");
		forward(toPage, request, response);

	}

	@Override
	protected void listDeal(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		List<Member> vDataList = null;
		
		String userName = request.getParameter("userName");
		request.setAttribute("userName", userName);

		PagerItem pagerItem = new PagerItem();
		pagerItem.parsePageSize(request.getParameter(pagerItem.getParamPageSize()));
		pagerItem.parsePageNum(request.getParameter(pagerItem.getParamPageNum()));

		Long rowCount = 0L;
		
		if (SysFun.isNullOrEmpty(userName)) {
			rowCount = memberService.count();
			pagerItem.changeRowCount(rowCount);
			vDataList = memberService.pager(pagerItem.getPageNum(), pagerItem.getPageSize());
			
		} else {
			rowCount = memberService.countByUserName(userName);
			pagerItem.changeRowCount(rowCount);
			vDataList = memberService.pagerByUserName(userName, pagerItem.getPageNum(), pagerItem.getPageSize());
		}

		pagerItem.changeUrl(SysFun.generalUrl(request.getRequestURI(), request.getQueryString()));

		request.setAttribute("pagerItem", pagerItem);
		request.setAttribute("DataList", vDataList);

//		String toPage = UIConst.VIEWPATH + "/Member_list.jsp";
//		request.getRequestDispatcher(toPage).forward(request, response);
		String toPage = getPagePath("Member", "list");
		forward(toPage, request, response);

	}

	@Override
	protected void insertView(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		String toPage = UIConst.VIEWPATH + "/Member_insert.jsp";
//		request.getRequestDispatcher(toPage).forward(request, response);
		String toPage = getPagePath("Member", "insert");
		forward(toPage, request, response);
	}

	@Override
	protected void insertDeal(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		HttpSession session = request.getSession();
		
		Member loginUser = (Member) session.getAttribute(UIConst.BG_LOGINUSER_KEY);
		
		String userName = request.getParameter("userName");
		String userPass = request.getParameter("userPass");
		String userPass2 = request.getParameter("userPass2");
		String nickName = request.getParameter("nickName");
		String email = request.getParameter("email");
		String mobile = request.getParameter("mobile");

		request.setAttribute("userName", userName);
		request.setAttribute("userPass", userPass);
		request.setAttribute("userPass2", userPass2);
		request.setAttribute("nickName", nickName);
		request.setAttribute("email", email);
		request.setAttribute("mobile", mobile);
		
		String vMsg = "";
		if (SysFun.isNullOrEmpty(userName)) {
			vMsg += "账号不能为空。";
		} else if (!Pattern.matches("^[A-Za-z0-9]{6,16}$", userName)) {
			vMsg += "账号只能包含字母、数字，长度在6-~16之间。";
		} else if (memberService.loadByUserName(userName) != null) {
			vMsg += "账号已存在。";
		}
		if (SysFun.isNullOrEmpty(userPass)) {
			vMsg += "密码不能为空。";
		} else if (!Pattern.matches("^[A-Za-z0-9]{6,16}$", userPass)) {
			vMsg += "密码只能包含字母、数字，长度在6-~16之间。";
		}
		if (SysFun.isNullOrEmpty(userPass2)) {
			vMsg += "确认密码不能为空。";
		} else if (!userPass2.equals(userPass)) {
			vMsg += "两次输入的密码不一致。";
		}
		if (SysFun.isNullOrEmpty(nickName)) {
			vMsg += "昵称不能为空。";
		} else if (!Pattern.matches("^[a-zA-Z0-9\\u4e00-\\u9fa5]+$", nickName)) {
			vMsg += "昵称只能包含字母、数字和汉字。";
		} else if (memberService.loadByNickName(nickName) != null) {
			vMsg += "昵称已存在。";
		}
		if (SysFun.isNullOrEmpty(email)) {
			vMsg += "邮箱不能为空。";
		} else if (!Pattern.matches("^[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", email)) {
			vMsg += "请输入有效的电子邮件地址。";
		} else if (memberService.loadByEmail(email) != null) {
			vMsg += "邮箱已存在。";
		}
		if (SysFun.isNullOrEmpty(mobile)) {
			mobile = "";
		} else if (!Pattern.matches("^(((13[0-9]{1})|(15[0-35-9]{1})|(17[0-9]{1})|(18[0-9]{1}))+\\d{8})$", mobile)) {
			vMsg += "电话号码格式不正确。";
		}
		
		if (!SysFun.isNullOrEmpty(vMsg)) {
			request.setAttribute("msg", vMsg);
			insertView(request, response);
			return;
		}
		
		Date date = new Date();
		Long userId = loginUser.getUserId();
		
		Member bean = new Member();
		bean.setUserName(userName);
		bean.setUserPass(userPass);
		bean.setNickName(nickName);
		bean.setEmail(email);
		bean.setMobile(mobile);
		bean.setStatus("");
		bean.setIsDeleted(0L);
		bean.setCreateBy(userId);
		bean.setUpdateBy(userId);
		bean.setCreateOn(date);
		bean.setUpdateOn(date);
		
		Long result = 0L;
		
		try {
			result = memberService.insert(bean);
		} catch(Exception ex) {
			vMsg = "添加失败。" + ex.getMessage();
		}
		
		if (result > 0) {
			out.println("<script>");
			out.println("parent.window.location.reload();");
			out.println("</script>");
		} else {
			request.setAttribute("msg", vMsg);
			insertView(request, response);
		}
		
	}

	@Override
	protected void updateView(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		String strUserId = request.getParameter("userId");
		if (!SysFun.isNullOrEmpty(strUserId)) {
			Long userId = Long.valueOf(strUserId);
			Member bean = memberService.load(userId);
			if (bean != null) {
				request.setAttribute("userId", userId);
				request.setAttribute("userName", bean.getUserName());
				request.setAttribute("userPass", bean.getUserPass());
				request.setAttribute("userPass2", bean.getUserPass());
				request.setAttribute("nickName", bean.getNickName());
				request.setAttribute("email", bean.getEmail());
				request.setAttribute("mobile", bean.getMobile());
//				String toPage = UIConst.VIEWPATH + "/Member_update.jsp";
//				request.getRequestDispatcher(toPage).forward(request, response);
				String toPage = getPagePath("Member", "update");
				forward(toPage, request, response);
				return;
			}
		}
		
		out.print("<script>");
		out.print("alert('数据不存在。');");
		out.print("parent.window.location.reload();");
		out.print("</script>");
		
	}

	@Override
	protected void updateDeal(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		HttpSession session = request.getSession();
		
		Member loginUser = (Member) session.getAttribute(UIConst.BG_LOGINUSER_KEY);
		
		Long userId = null;
		String strUserId = request.getParameter("userId");
		String userName = request.getParameter("userName");
		String userPass = request.getParameter("userPass");
		String userPass2 = request.getParameter("userPass2");
		String nickName = request.getParameter("nickName");
		String email = request.getParameter("email");
		String mobile = request.getParameter("mobile");

		request.setAttribute("userId", strUserId);
		request.setAttribute("userName", userName);
		request.setAttribute("userPass", userPass);
		request.setAttribute("userPass2", userPass2);
		request.setAttribute("nickName", nickName);
		request.setAttribute("email", email);
		request.setAttribute("mobile", mobile);
		
		String vMsg = "";
		if (SysFun.isNullOrEmpty(strUserId)) {
			vMsg += "用户ID不能为空。";
		} else {
			userId = Long.valueOf(strUserId);
			if (memberService.load(userId) == null) {
				vMsg += "数据不存在。";
			}
		}
		if (SysFun.isNullOrEmpty(userName)) {
			vMsg += "账号不能为空。";
		} else if (!Pattern.matches("^[A-Za-z0-9]{6,16}$", userName)) {
			vMsg += "账号只能包含字母、数字，长度在6-~16之间。";
		} else {
			Member bean = memberService.loadByUserName(userName);
			if (bean != null && !bean.getUserId().equals(userId)) {
				vMsg += "账号已存在。";
			}
		}
		if (SysFun.isNullOrEmpty(userPass)) {
			vMsg += "密码不能为空。";
		} else if (!Pattern.matches("^[A-Za-z0-9]{6,16}$", userPass)) {
			vMsg += "密码只能包含字母、数字，长度在6-~16之间。";
		}
		if (SysFun.isNullOrEmpty(userPass2)) {
			vMsg += "确认密码不能为空。";
		} else if (!userPass2.equals(userPass)) {
			vMsg += "两次输入的密码不一致。";
		}
		if (SysFun.isNullOrEmpty(nickName)) {
			vMsg += "昵称不能为空。";
		} else if (!Pattern.matches("^[a-zA-Z0-9\\u4e00-\\u9fa5]+$", nickName)) {
			vMsg += "昵称只能包含字母、数字和汉字。";
		} else {
			Member bean = memberService.loadByNickName(nickName);
			if (bean != null && !bean.getUserId().equals(userId)) {
				vMsg += "昵称已存在。";
			}
		}
		if (SysFun.isNullOrEmpty(email)) {
			vMsg += "邮箱不能为空。";
		} else if (!Pattern.matches("^[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", email)) {
			vMsg += "请输入有效的电子邮件地址。";
		} else {
			Member bean = memberService.loadByEmail(email);
			if (bean != null && !bean.getUserId().equals(userId)) {
				vMsg += "邮箱已存在。";
			}
		}
		if (SysFun.isNullOrEmpty(mobile)) {
			mobile = "";
		} else if (!Pattern.matches("^(((13[0-9]{1})|(15[0-35-9]{1})|(17[0-9]{1})|(18[0-9]{1}))+\\d{8})$", mobile)) {
			vMsg += "电话号码格式不正确。";
		}
		
		if (!SysFun.isNullOrEmpty(vMsg)) {
			request.setAttribute("msg", vMsg);
			insertView(request, response);
			return;
		}
		
		Date date = new Date();
		Long loginUserId = loginUser.getUserId();
		
		Member bean = new Member();
		bean.setUserId(userId);
		bean.setUserName(userName);
		bean.setUserPass(userPass);
		bean.setNickName(nickName);
		bean.setEmail(email);
		bean.setMobile(mobile);
		bean.setUpdateBy(loginUserId);
		bean.setUpdateOn(date);
		
		Long result = memberService.update(bean);

		if (result > 0) {
			out.println("<script>");
			out.println("parent.window.location.reload();");
			out.println("</script>");
		} else {
			request.setAttribute("msg", "修改失败");
			insertView(request, response);
		}
		
	}

	@Override
	protected void deleteDeal(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();

		String strUserId = request.getParameter("userId");
		if (!SysFun.isNullOrEmpty(strUserId)) {
			Long userId = Long.valueOf(strUserId);

			Long result = memberService.delete(userId);
			if (result > 0) {
				out.print("ok");
				return;
			}
		}

		out.print("nook");
	}

	@Override
	protected void deleteNotAjax(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		PrintWriter out = response.getWriter();

		String strUserId = request.getParameter("userId");
		if (!SysFun.isNullOrEmpty(strUserId)) {
			Long userId = Long.valueOf(strUserId);

			Long result = memberService.delete(userId);
			if (result > 0) {
				out.print("<script>");
				out.print("alert('删除成功');");
				out.print("location.href='Member?oper=list';");
				out.print("</script>");
				return;
			}
		}

		out.print("<script>");
		out.print("alert('删除失败');");
		out.print("location.href='Member?oper=list';");
		out.print("</script>");

	}

	@Override
	protected void detailView(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		PrintWriter out = response.getWriter();
		
		Long userId = null;
		String strUserId = request.getParameter("userId");
		
		if (!SysFun.isNullOrEmpty(strUserId)) {
			userId = Long.valueOf(strUserId);
			Member bean = memberService.load(userId);
			if (bean != null) {
				request.setAttribute("bean", bean);
				
//				String toPage = UIConst.VIEWPATH + "/Member_detail.jsp";
//				request.getRequestDispatcher(toPage).forward(request, response);
				String toPage = getPagePath("Member", "detail");
				forward(toPage, request, response);
				return;
			}
		}
		
		out.print("<script>");
		out.print("alert('数据不存在。');");
		out.print("parent.window.location.reload();");
		out.print("</script>");
		
	}

}
