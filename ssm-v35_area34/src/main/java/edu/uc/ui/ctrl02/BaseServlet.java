package edu.uc.ui.ctrl02;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public abstract class BaseServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	protected String areaName = "/" + UIConst.AREANAME;
	
	public String getAreaName() {
		return areaName;
	}
	
	private String areaPath;
	
	public String getAreaPath() {
		return areaPath;
	}
	
	public String currAreaPath(HttpServletRequest request) {
		if (areaPath == null || areaPath.isEmpty()) {
			areaPath = request.getServletContext().getContextPath() + getAreaName();
			request.setAttribute("areaName", areaName);
			request.setAttribute("areaPath", areaPath);
		}
		return areaPath;
	}
	
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		currAreaPath(request);
		super.service(request, response);
	}
	
	public String Page_Prefix = "/WEB-INF/" + UIConst.VIEWNAME + "/";
	public String Page_Suffix = ".jsp";
	public String Page_Format = Page_Prefix + "%s" + Page_Suffix;
	
	public String getPagePath(String pFileName) {
		return String .format(Page_Format, pFileName);
	}
	
	public String getPagePath(String pBeanName, String pOperName) {
		return String.format(Page_Format, pBeanName + "_" + pOperName);
	}
	
	public String Url_Prefix = "/";
	public String Url_Suffix = "";
	public String Url_Format = Url_Prefix + "%s" + Url_Suffix;
	
	public String getUrlPath(String pFileName) {
		return getAreaPath() + String .format(Url_Format, pFileName);
	}
	
	public String getUrlPath(String pBeanName, String pOperName) {
		return getAreaPath() + String.format(Url_Format, pBeanName + "_" + pOperName);
	}
	
	protected boolean checkLogin(HttpServletRequest request, HttpServletResponse response) throws IOException {
		boolean vIsOK = false;
		
		if (request.getSession().getAttribute(UIConst.BG_LOGINUSER_KEY) == null) {
			response.sendRedirect(getAreaPath() + "/Login");
		} else {
			vIsOK = true;
		}
		
		return vIsOK;
	}

	protected boolean checkAdmin(HttpServletRequest request, HttpServletResponse response) throws IOException {
		boolean vIsOK = false;
		
		if (request.getSession().getAttribute(UIConst.BG_ISADMIN_KEY) != null) {
			vIsOK = true;
		}
		
		return vIsOK;
	}
	
	protected void forward(String page, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher(page).forward(request, response);
	}
	
	protected void redirect(String url, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect(url);
	}
	
	protected void redirectAreaPath(String url, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect(getAreaPath() + url);
	}

}
