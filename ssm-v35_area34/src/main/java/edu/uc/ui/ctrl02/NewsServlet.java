package edu.uc.ui.ctrl02;

import com.liuvei.common.PagerItem;
import com.liuvei.common.SysFun;
import edu.uc.bean.Member;
import edu.uc.bean.News;
import edu.uc.bean.NewsCat;
import edu.uc.bean.NewsExCont;
import edu.uc.service.NewsCatService;
import edu.uc.service.NewsCommentService;
import edu.uc.service.NewsExContService;
import edu.uc.service.NewsService;
import edu.util.SpringUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;

@WebServlet(UIConst.AREAPATH + "/News")
public class NewsServlet extends CrudServlet {

	private static final long serialVersionUID = 1L;

	private NewsService newsService = SpringUtil.getBean("newsService", NewsService.class);
	private NewsCatService newsCatService = SpringUtil.getBean("newsCatService", NewsCatService.class);
	private NewsCommentService newsCommentService = SpringUtil.getBean("newsCommentService", NewsCommentService.class);
	private NewsExContService newsExContService = SpringUtil.getBean("newsExContService", NewsExContService.class);

	@Override
	protected void listView(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		List<News> vDataList = null;

		PagerItem pagerItem = new PagerItem();
		pagerItem.parsePageSize(request.getParameter(pagerItem.getParamPageSize()));
		pagerItem.parsePageNum(request.getParameter(pagerItem.getParamPageNum()));

		Long rowCount = 0L;
		rowCount = newsService.count();
		pagerItem.changeRowCount(rowCount);

		vDataList = newsService.pager(pagerItem.getPageNum(), pagerItem.getPageSize());

		pagerItem.changeUrl(SysFun.generalUrl(request.getRequestURI(), request.getQueryString()));

		request.setAttribute("pagerItem", pagerItem);
		request.setAttribute("DataList", vDataList);

//		String toPage = UIConst.VIEWPATH + "/News_list.jsp";
//		request.getRequestDispatcher(toPage).forward(request, response);
		String toPage = getPagePath("News", "list");
		forward(toPage, request, response);

	}

	@Override
	protected void listDeal(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		List<News> vDataList = null;
		
		String title = request.getParameter("title");
		request.setAttribute("title", title);

		PagerItem pagerItem = new PagerItem();
		pagerItem.parsePageSize(request.getParameter(pagerItem.getParamPageSize()));
		pagerItem.parsePageNum(request.getParameter(pagerItem.getParamPageNum()));

		Long rowCount = 0L;
		
		if (SysFun.isNullOrEmpty(title)) {
			rowCount = newsService.count();
			pagerItem.changeRowCount(rowCount);
			vDataList = newsService.pager(pagerItem.getPageNum(), pagerItem.getPageSize());
			
		} else {
			rowCount = newsService.countByTitle(title);
			pagerItem.changeRowCount(rowCount);
			vDataList = newsService.pagerByTitle(title, pagerItem.getPageNum(), pagerItem.getPageSize());
		}

		pagerItem.changeUrl(SysFun.generalUrl(request.getRequestURI(), request.getQueryString()));

		request.setAttribute("pagerItem", pagerItem);
		request.setAttribute("DataList", vDataList);

//		String toPage = UIConst.VIEWPATH + "/News_list.jsp";
//		request.getRequestDispatcher(toPage).forward(request, response);
		String toPage = getPagePath("News", "list");
		forward(toPage, request, response);

	}

	@Override
	protected void insertView(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		List<NewsCat> vDataList = newsCatService.list();
		request.setAttribute("dataList", vDataList);
		
//		String toPage = UIConst.VIEWPATH + "/News_insert.jsp";
//		request.getRequestDispatcher(toPage).forward(request, response);
		String toPage = getPagePath("News", "insert");
		forward(toPage, request, response);
	}

	@Override
	protected void insertDeal(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		HttpSession session = request.getSession();
		
		Member loginUser = (Member) session.getAttribute(UIConst.BG_LOGINUSER_KEY);
		
		Long catId = null;
		String strCatId = request.getParameter("catId");
		String title = request.getParameter("title");
		String author = request.getParameter("author");
		String summary = request.getParameter("summary");
		String content = request.getParameter("content");

		request.setAttribute("catId", catId);
		request.setAttribute("title", title);
		request.setAttribute("author", author);
		request.setAttribute("summary", summary);
		request.setAttribute("content", content);
		
		String vMsg = "";
		if (SysFun.isNullOrEmpty(strCatId)) {
			vMsg += "请选择类目。";
		} else {
			catId = Long.valueOf(strCatId);
			if (newsCatService.load(catId) == null) {
				vMsg += "类目不存在。";
			}
		}
		if (SysFun.isNullOrEmpty(title)) {
			vMsg += "标题不能为空。";
		}
		if (SysFun.isNullOrEmpty(author)) {
			vMsg += "作者不能为空。";
		}
		if (SysFun.isNullOrEmpty(summary)) {
			vMsg += "摘要不能为空。";
		}
		
		if (!SysFun.isNullOrEmpty(vMsg)) {
			request.setAttribute("msg", vMsg);
			insertView(request, response);
			return;
		}
		
		Date date = new Date();
		Long userId = loginUser.getUserId();
		
		News bean = new News();
		bean.setCatId(catId);
		bean.setTitle(title);
		bean.setAuthor(author);
		bean.setSummary(summary);
		bean.setStatus("");
		bean.setIsDeleted(0L);
		bean.setCreateBy(userId);
		bean.setUpdateBy(userId);
		bean.setCreateOn(date);
		bean.setUpdateOn(date);
		
		Long result = 0L;
		
		try {
			result = newsService.insert(bean);
		} catch(Exception ex) {
			vMsg = "添加失败。" + ex.getMessage();
		}
		
		if (result > 0) {
			
			NewsExCont newsExCont = new NewsExCont();
			newsExCont.setNewsId(bean.getNewsId());
			newsExCont.setContent(content);
			newsExCont.setStatus("");
			newsExCont.setIsDeleted(0L);
			newsExCont.setCreateBy(userId);
			newsExCont.setUpdateBy(userId);
			newsExCont.setCreateOn(date);
			newsExCont.setUpdateOn(date);
			
			newsExContService.insert(newsExCont);
			
			out.println("<script>");
			out.println("parent.window.location.reload();");
			out.println("</script>");
		} else {
			request.setAttribute("msg", vMsg);
			insertView(request, response);
		}
		
	}

	@Override
	protected void updateView(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		String strNewsId = request.getParameter("newsId");
		if (!SysFun.isNullOrEmpty(strNewsId)) {
			Long newsId = Long.valueOf(strNewsId);
			News bean = newsService.load(newsId);
			if (bean != null) {
				
				List<NewsCat> vDataList = newsCatService.list();
				request.setAttribute("dataList", vDataList);
				
				request.setAttribute("newsId", newsId);
				request.setAttribute("catId", bean.getCatId());
				request.setAttribute("title", bean.getTitle());
				request.setAttribute("author", bean.getAuthor());
				request.setAttribute("summary", bean.getSummary());
				request.setAttribute("content", bean.getNewsExCont().getContent());
//				String toPage = UIConst.VIEWPATH + "/News_update.jsp";
//				request.getRequestDispatcher(toPage).forward(request, response);
				String toPage = getPagePath("News", "update");
				forward(toPage, request, response);
				return;
			}
		}
		
		out.print("<script>");
		out.print("alert('数据不存在。');");
		out.print("parent.window.location.reload();");
		out.print("</script>");
		
	}

	@Override
	protected void updateDeal(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		HttpSession session = request.getSession();
		
		Member loginUser = (Member) session.getAttribute(UIConst.BG_LOGINUSER_KEY);
		
		Long newsId = null;
		String strNewsId = request.getParameter("newsId");
		Long catId = null;
		String strCatId = request.getParameter("catId");
		String title = request.getParameter("title");
		String author = request.getParameter("author");
		String summary = request.getParameter("summary");
		String content = request.getParameter("content");

		request.setAttribute("newsId", strNewsId);
		request.setAttribute("catId", catId);
		request.setAttribute("title", title);
		request.setAttribute("author", author);
		request.setAttribute("summary", summary);
		request.setAttribute("content", content);
		
		String vMsg = "";
		if (SysFun.isNullOrEmpty(strNewsId)) {
			vMsg += "新闻ID不能为空。";
		} else {
			newsId = Long.valueOf(strNewsId);
			if (newsService.load(newsId) == null) {
				vMsg += "数据不存在。";
			}
		}
		if (SysFun.isNullOrEmpty(strCatId)) {
			vMsg += "请选择类目。";
		} else {
			catId = Long.valueOf(strCatId);
			if (newsCatService.load(catId) == null) {
				vMsg += "类目不存在。";
			}
		}
		if (SysFun.isNullOrEmpty(title)) {
			vMsg += "标题不能为空。";
		}
		if (SysFun.isNullOrEmpty(author)) {
			vMsg += "作者不能为空。";
		}
		if (SysFun.isNullOrEmpty(summary)) {
			vMsg += "摘要不能为空。";
		}
		
		if (!SysFun.isNullOrEmpty(vMsg)) {
			request.setAttribute("msg", vMsg);
			insertView(request, response);
			return;
		}
		
		Date date = new Date();
		Long userId = loginUser.getUserId();
		
		News bean = new News();
		bean.setNewsId(newsId);
		bean.setCatId(catId);
		bean.setTitle(title);
		bean.setAuthor(author);
		bean.setSummary(summary);
		bean.setUpdateBy(userId);
		bean.setUpdateOn(date);
		
		Long result = newsService.update(bean);

		if (result > 0) {
			
			NewsExCont newsExCont = new NewsExCont();
			newsExCont.setNewsId(newsId);
			newsExCont.setContent(content);
			newsExCont.setUpdateBy(userId);
			newsExCont.setUpdateOn(date);
			
			newsExContService.update(newsExCont);
			
			out.println("<script>");
			out.println("parent.window.location.reload();");
			out.println("</script>");
		} else {
			request.setAttribute("msg", "修改失败");
			insertView(request, response);
		}
		
	}

	@Override
	protected void deleteDeal(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();

		String strNewsId = request.getParameter("newsId");
		if (!SysFun.isNullOrEmpty(strNewsId)) {
			Long newsId = Long.valueOf(strNewsId);

			Long result = newsService.delete(newsId);
			if (result > 0) {
				newsCommentService.deleteByNewsId(newsId);
				newsExContService.deleteByNewsId(newsId);
				out.print("ok");
				return;
			}
		}

		out.print("nook");
	}

	@Override
	protected void deleteNotAjax(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		PrintWriter out = response.getWriter();

		String strNewsId = request.getParameter("newsId");
		System.out.println(strNewsId);
		if (!SysFun.isNullOrEmpty(strNewsId)) {
			Long newsId = Long.valueOf(strNewsId);

			Long result = newsService.delete(newsId);
			if (result > 0) {
				newsCommentService.deleteByNewsId(newsId);
				newsExContService.deleteByNewsId(newsId);
				out.print("<script>");
				out.print("alert('删除成功');");
				out.print("location.href='News?oper=list';");
				out.print("</script>");
				return;
			}
		}

		out.print("<script>");
		out.print("alert('删除失败');");
		out.print("location.href='News?oper=list';");
		out.print("</script>");

	}

	@Override
	protected void detailView(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		PrintWriter out = response.getWriter();
		
		Long newsId = null;
		String strNewsId = request.getParameter("newsId");
		
		if (!SysFun.isNullOrEmpty(strNewsId)) {
			newsId = Long.valueOf(strNewsId);
			News bean = newsService.load(newsId);
			if (bean != null) {
				request.setAttribute("bean", bean);
				request.setAttribute("catName", newsCatService.load(bean.getCatId()).getCatName());
				
//				String toPage = UIConst.VIEWPATH + "/News_detail.jsp";
//				request.getRequestDispatcher(toPage).forward(request, response);
				String toPage = getPagePath("News", "detail");
				forward(toPage, request, response);
				return;
			}
		}
		
		out.print("<script>");
		out.print("alert('数据不存在。');");
		out.print("parent.window.location.reload();");
		out.print("</script>");
		
	}

}
