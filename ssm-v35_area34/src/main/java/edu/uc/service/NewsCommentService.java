package edu.uc.service;

import edu.uc.bean.NewsComment;

import java.util.List;

public interface NewsCommentService extends BaseService<NewsComment> {

    Long deleteByNewsId(Long newsId);

    Long countByNewsId(Long newsId);

    Long countByAuthor(String author);

    Long countByAuthorAndNewsId(String author, Long newsId);

    List<NewsComment> pagerByNewsId(Long newsId, Long pageNum, Long pageSize);

    List<NewsComment> pagerByAuthor(String author, Long pageNum, Long pageSize);

    List<NewsComment> pagerByAuthorAndNewsId(String author, Long newsId, Long pageNum, Long pageSize);

}
