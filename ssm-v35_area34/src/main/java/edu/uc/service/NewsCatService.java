package edu.uc.service;

import edu.uc.bean.NewsCat;

import java.util.List;

public interface NewsCatService extends BaseService<NewsCat> {

    NewsCat loadByCatName(String catName);

    Long countByCatName(String catName);

    List<NewsCat> pagerByCatName(String catName, Long pageNum, Long pageSize);

}
