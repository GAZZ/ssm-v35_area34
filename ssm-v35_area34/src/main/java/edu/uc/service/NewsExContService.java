package edu.uc.service;

import edu.uc.bean.NewsExCont;

import java.util.List;

public interface NewsExContService extends BaseService<NewsExCont> {

    Long deleteByNewsId(Long newsId);

    NewsExCont loadByNewsId(Long newsId);

    Long countByNewsId(Long newsId);

    List<NewsExCont> pagerByNewsId(Long newsId, Long pageNum, Long pageSize);

}
