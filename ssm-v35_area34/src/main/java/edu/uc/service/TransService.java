package edu.uc.service;

public interface TransService {

    Boolean testTxInit(Long userA, Long userB, Long money);

    Boolean showMoney(Long userA, Long userB);

    Boolean testTxYes(Long userA, Long userB, Long money);

    Boolean testTxNo(Long userA, Long userB, Long money);
}
