package edu.uc.service;

import edu.uc.bean.Member;

import java.util.List;

public interface MemberService extends BaseService<Member> {

    Member loadByUserName(String userName);

    Member loadByNickName(String nickName);

    Member loadByEmail(String email);

    Long countByUserName(String userName);

    List<Member> pagerByUserName(String userName, Long pageNum, Long pageSize);

}
