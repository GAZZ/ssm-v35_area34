package edu.uc.service.impl;

import edu.uc.bean.Member;
import edu.uc.mapper.MemberMapper;
import edu.uc.service.MemberService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("memberService")
public class MemberServiceImpl extends BaseServiceImpl<Member> implements MemberService {

	@Resource
	private MemberMapper memberMapper;

	@Override
	public Long insert(Member bean) {
		return memberMapper.insert(bean);
	}

	@Override
	public Long delete(Long id) {
		return memberMapper.delete(id);
	}

	@Override
	public Long update(Member bean) {
		return memberMapper.update(bean);
	}

	@Override
	public List<Member> list() {
		return memberMapper.list();
	}

	@Override
	public Member load(Long id) {
		return memberMapper.load(id);
	}

	@Override
	public Member loadByUserName(String userName) {
		return memberMapper.loadByUserName(userName);
	}

	@Override
	public Member loadByNickName(String nickName) {
		return memberMapper.loadByNickName(nickName);
	}

	@Override
	public Member loadByEmail(String email) {
		return memberMapper.loadByEmail(email);
	}

	@Override
	public Long count() {
		return memberMapper.count();
	}

	@Override
	public Long countByUserName(String userName) {
		return memberMapper.countByUserName(userName);
	}

	@Override
	public List<Member> pager(Long pageNum, Long pageSize) {
		pageNum = (pageNum - 1) * pageSize;
		return memberMapper.pager(pageNum, pageSize);
	}

	@Override
	public List<Member> pagerByUserName(String userName, Long pageNum, Long pageSize) {
		pageNum = (pageNum - 1) * pageSize;
		return memberMapper.pagerByUserName(userName, pageNum, pageSize);
	}

}
