package edu.uc.service.impl;

import edu.uc.bean.NewsExCont;
import edu.uc.mapper.NewsExContMapper;
import edu.uc.service.NewsExContService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("newsExContService")
public class NewsExContServiceImpl extends BaseServiceImpl<NewsExCont> implements NewsExContService {

	@Resource
	private NewsExContMapper newsExContMapper;

	@Override
	public Long insert(NewsExCont bean) {
		return newsExContMapper.insert(bean);
	}

	@Override
	public Long delete(Long id) {
		return newsExContMapper.delete(id);
	}

	@Override
	public Long deleteByNewsId(Long newsId) {
		return newsExContMapper.deleteByNewsId(newsId);
	}

	@Override
	public Long update(NewsExCont bean) {
		return newsExContMapper.update(bean);
	}

	@Override
	public List<NewsExCont> list() {
		return newsExContMapper.list();
	}

	@Override
	public NewsExCont load(Long id) {
		return newsExContMapper.load(id);
	}

	@Override
	public NewsExCont loadByNewsId(Long newsId) {
		return newsExContMapper.loadByNewsId(newsId);
	}

	@Override
	public Long count() {
		return newsExContMapper.count();
	}

	@Override
	public Long countByNewsId(Long newsId) {
		return newsExContMapper.countByNewsId(newsId);
	}

	@Override
	public List<NewsExCont> pager(Long pageNum, Long pageSize) {
		pageNum = (pageNum - 1) * pageSize;
		return newsExContMapper.pager(pageNum, pageSize);
	}

	@Override
	public List<NewsExCont> pagerByNewsId(Long newsId, Long pageNum, Long pageSize) {
		pageNum = (pageNum - 1) * pageSize;
		return newsExContMapper.pagerByNewsId(newsId, pageNum, pageSize);
	}

}
