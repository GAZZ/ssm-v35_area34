package edu.uc.service.impl;

import edu.uc.bean.News;
import edu.uc.mapper.NewsCatMapper;
import edu.uc.mapper.NewsExContMapper;
import edu.uc.mapper.NewsMapper;
import edu.uc.service.NewsService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("newsService")
public class NewsServiceImpl extends BaseServiceImpl<News> implements NewsService {

    @Resource
    private NewsMapper newsMapper;

    @Resource
    private NewsExContMapper newsExContMapper;

    @Resource
    private NewsCatMapper newsCatMapper;

    private List<News> listRelation(List<News> list) {
        if (list != null) {
            for (News item : list) {
                item.setNewsCat(newsCatMapper.load(item.getCatId()));
                item.setNewsExCont(newsExContMapper.loadByNewsId(item.getNewsId()));
            }
        }
        return list;
    }

    private News relation(News bean) {
        if (bean != null) {
            bean.setNewsCat(newsCatMapper.load(bean.getCatId()));
            bean.setNewsExCont(newsExContMapper.loadByNewsId(bean.getNewsId()));
        }
        return bean;
    }

    @Override
    public Long insert(News bean) {
        return newsMapper.insert(bean);
    }

    @Override
    public Long delete(Long id) {
        return newsMapper.delete(id);
    }

    @Override
    public Long update(News bean) {
        return newsMapper.update(bean);
    }

    @Override
    public List<News> list() {
        return newsMapper.list();
    }

    @Override
    public News load(Long id) {
        return relation(newsMapper.load(id));
    }

    @Override
    public News loadByTitle(String title) {
        return relation(newsMapper.loadByTitle(title));
    }

    @Override
    public Long count() {
        return newsMapper.count();
    }

    @Override
    public Long countByCatId(Long carId) {
        return newsMapper.countByCatId(carId);
    }

    @Override
    public Long countByAuthor(String author) {
        return newsMapper.countByAuthor(author);
    }

    @Override
    public Long countByTitle(String title) {
        return newsMapper.countByTitle(title);
    }

    @Override
    public List<News> pager(Long pageNum, Long pageSize) {
        pageNum = (pageNum - 1) * pageSize;
        return listRelation(newsMapper.pager(pageNum, pageSize));
    }

    @Override
    public List<News> pagerByTitle(String title, Long pageNum, Long pageSize) {
        pageNum = (pageNum - 1) * pageSize;
        return listRelation(newsMapper.pagerByTitle(title, pageNum, pageSize));
    }

}
