package edu.uc.service.impl;

import edu.uc.bean.Member;
import edu.uc.mapper.MemberMapper;
import edu.uc.service.TransService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service("transService")
public class TransServiceImpl implements TransService {

    @Resource
    private MemberMapper memberMapper;

    @Override
    public Boolean testTxInit(Long userA, Long userB, Long money) {
        Boolean isOK = false;

        Member beanA = memberMapper.load(userA);
        Member beanB = memberMapper.load(userB);

        if (beanA != null && beanB != null) {
            beanA.setSortNum(money);
            beanB.setSortNum(money);
            memberMapper.update(beanA);
            memberMapper.update(beanB);

            System.out.println("--------------------");
            System.out.println("初始化完毕");
            System.out.println("--------------------");

            isOK = true;
        }

        return isOK;
    }

    @Override
    public Boolean showMoney(Long userA, Long userB) {
        Boolean isOK = false;

        Member beanA = memberMapper.load(userA);
        Member beanB = memberMapper.load(userB);

        if (beanA != null && beanB != null) {
            System.out.println("--------------------");
            System.out.println("账号【" + beanA + "】的余额为：" + beanA.getSortNum());
            System.out.println("账号【" + beanB + "】的余额为：" + beanB.getSortNum());
            System.out.println("--------------------");

            isOK = true;
        }

        return isOK;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Boolean testTxYes(Long userA, Long userB, Long money) {
        Boolean isOK = false;

        Member beanA = memberMapper.load(userA);
        Member beanB = memberMapper.load(userB);

        try {
            if (beanA != null && beanB != null) {
                if (beanA.getSortNum() >= money) {
                    beanA.setSortNum(beanA.getSortNum() - money);
                    beanB.setSortNum(beanB.getSortNum() + money);
                    memberMapper.update(beanA);
                    int result = 100 / 0;
                    memberMapper.update(beanB);
                    System.out.println("--------------------");
                    System.out.println("textTxYes完毕");
                    System.out.println("--------------------");

                    isOK = true;
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return isOK;
    }

    @Override
    public Boolean testTxNo(Long userA, Long userB, Long money) {
        Boolean isOK = false;

        Member beanA = memberMapper.load(userA);
        Member beanB = memberMapper.load(userB);

        if (beanA != null && beanB != null) {
            if (beanA.getSortNum() >= money) {
                beanA.setSortNum(beanA.getSortNum() - money);
                beanB.setSortNum(beanB.getSortNum() + money);
                memberMapper.update(beanA);
                int result = 100 / 0;
                memberMapper.update(beanB);
                System.out.println("--------------------");
                System.out.println("testTxNo完毕");
                System.out.println("--------------------");

                isOK = true;
            }
        }

        return isOK;
    }
}
