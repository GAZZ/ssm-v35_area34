package edu.uc.service.impl;

import edu.uc.bean.NewsComment;
import edu.uc.mapper.NewsCommentMapper;
import edu.uc.service.NewsCommentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("newsCommentService")
public class NewsCommentServiceImpl extends BaseServiceImpl<NewsComment> implements NewsCommentService {

	@Resource
	private NewsCommentMapper newsCommentMapper;

	@Override
	public Long insert(NewsComment bean) {
		return newsCommentMapper.insert(bean);
	}

	@Override
	public Long delete(Long id) {
		return newsCommentMapper.delete(id);
	}

	@Override
	public Long deleteByNewsId(Long newsId) {
		return newsCommentMapper.deleteByNewsId(newsId);
	}

	@Override
	public Long update(NewsComment bean) {
		return newsCommentMapper.update(bean);
	}

	@Override
	public List<NewsComment> list() {
		return newsCommentMapper.list();
	}

	@Override
	public NewsComment load(Long id) {
		return newsCommentMapper.load(id);
	}

	@Override
	public Long count() {
		return newsCommentMapper.count();
	}

	@Override
	public Long countByNewsId(Long newsId) {
		return newsCommentMapper.countByNewsId(newsId);
	}

	@Override
	public Long countByAuthor(String author) {
		return newsCommentMapper.countByAuthor(author);
	}

	@Override
	public Long countByAuthorAndNewsId(String author, Long newsId) {
		return newsCommentMapper.countByAuthorAndNewsId(author, newsId);
	}

	@Override
	public List<NewsComment> pager(Long pageNum, Long pageSize) {
		pageNum = (pageNum - 1) * pageSize;
		return newsCommentMapper.pager(pageNum, pageSize);
	}

	@Override
	public List<NewsComment> pagerByNewsId(Long newsId, Long pageNum, Long pageSize) {
		pageNum = (pageNum - 1) * pageSize;
		return newsCommentMapper.pagerByNewsId(newsId, pageNum, pageSize);
	}

	@Override
	public List<NewsComment> pagerByAuthor(String author, Long pageNum, Long pageSize) {
		pageNum = (pageNum - 1) * pageSize;
		return newsCommentMapper.pagerByAuthor(author, pageNum, pageSize);
	}

	@Override
	public List<NewsComment> pagerByAuthorAndNewsId(String author, Long newsId, Long pageNum, Long pageSize) {
		pageNum = (pageNum - 1) * pageSize;
		return newsCommentMapper.pagerByAuthorAndNewsId(author, newsId, pageNum, pageSize);
	}

}
