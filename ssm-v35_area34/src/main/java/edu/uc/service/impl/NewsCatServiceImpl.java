package edu.uc.service.impl;

import edu.uc.bean.NewsCat;
import edu.uc.mapper.NewsCatMapper;
import edu.uc.service.NewsCatService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("newsCatService")
public class NewsCatServiceImpl extends BaseServiceImpl<NewsCat> implements NewsCatService {

	@Resource
	private NewsCatMapper newsCatMapper;

	@Override
	public Long insert(NewsCat bean) {
		return newsCatMapper.insert(bean);
	}

	@Override
	public Long delete(Long id) {
		return newsCatMapper.delete(id);
	}

	@Override
	public Long update(NewsCat bean) {
		return newsCatMapper.update(bean);
	}

	@Override
	public List<NewsCat> list() {
		return newsCatMapper.list();
	}

	@Override
	public NewsCat load(Long id) {
		return newsCatMapper.load(id);
	}

	@Override
	public NewsCat loadByCatName(String catName) {
		return newsCatMapper.loadByCatName(catName);
	}

	@Override
	public Long count() {
		return newsCatMapper.count();
	}

	@Override
	public Long countByCatName(String catName) {
		return newsCatMapper.countByCatName(catName);
	}

	@Override
	public List<NewsCat> pager(Long pageNum, Long pageSize) {
		pageNum = (pageNum - 1) * pageSize;
		return newsCatMapper.pager(pageNum, pageSize);
	}

	@Override
	public List<NewsCat> pagerByCatName(String catName, Long pageNum, Long pageSize) {
		pageNum = (pageNum - 1) * pageSize;
		return newsCatMapper.pagerByCatName(catName, pageNum, pageSize);
	}

}
