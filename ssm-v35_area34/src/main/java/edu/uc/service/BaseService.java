package edu.uc.service;

import java.util.List;

public interface BaseService<T> {

	Long insert(T bean);

	Long delete(Long id);

	Long update(T bean);

	List<T> list();

	T load(Long id);

	Long count();

	List<T> pager(Long pageNum, Long pageSize);

}
