package edu.uc.service;

import edu.uc.bean.News;

import java.util.List;

public interface NewsService extends BaseService<News> {

    News loadByTitle(String title);

    Long countByCatId(Long carId);

    Long countByAuthor(String author);

    Long countByTitle(String title);

    List<News> pagerByTitle(String title, Long pageNum, Long pageSize);

}
