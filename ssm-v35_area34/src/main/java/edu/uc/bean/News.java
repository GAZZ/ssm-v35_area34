package edu.uc.bean;

import java.util.Date;

public class News implements java.io.Serializable {

	private static final long serialVersionUID = -1384945112846306211L;
	
	private Long newsId;
	private Long catId;
	private String title;
	private String author;
	private String summary;
	private String picPath;
	private String status;
	private String remark;
	private Long sortNum;
	private Long isDeleted;
	private Long createBy;
	private Long updateBy;
	private Date createOn;
	private Date updateOn;
	
	private NewsCat newsCat;
	private NewsExCont newsExCont;

	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	public Long getCatId() {
		return catId;
	}

	public void setCatId(Long catId) {
		this.catId = catId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getPicPath() {
		return picPath;
	}

	public void setPicPath(String picPath) {
		this.picPath = picPath;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Long getSortNum() {
		return sortNum;
	}

	public void setSortNum(Long sortNum) {
		this.sortNum = sortNum;
	}

	public Long getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Long isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Long getCreateBy() {
		return createBy;
	}

	public void setCreateBy(Long createBy) {
		this.createBy = createBy;
	}

	public Long getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(Long updateBy) {
		this.updateBy = updateBy;
	}

	public Date getCreateOn() {
		return createOn;
	}

	public void setCreateOn(Date createOn) {
		this.createOn = createOn;
	}

	public Date getUpdateOn() {
		return updateOn;
	}

	public void setUpdateOn(Date updateOn) {
		this.updateOn = updateOn;
	}

	public NewsCat getNewsCat() {
		return newsCat;
	}

	public void setNewsCat(NewsCat newsCat) {
		this.newsCat = newsCat;
	}

	public NewsExCont getNewsExCont() {
		return newsExCont;
	}

	public void setNewsExCont(NewsExCont newsExCont) {
		this.newsExCont = newsExCont;
	}

}
