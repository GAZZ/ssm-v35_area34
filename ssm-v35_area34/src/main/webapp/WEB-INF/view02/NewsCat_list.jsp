﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<!--[if lt IE 9]>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/H-ui.admin/lib/html5shiv.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/H-ui.admin/lib/respond.min.js"></script>
<![endif]-->
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/H-ui.admin/static/h-ui/css/H-ui.min.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/H-ui.admin/static/h-ui.admin/css/H-ui.admin.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/H-ui.admin/lib/Hui-iconfont/1.0.8/iconfont.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/H-ui.admin/static/h-ui.admin/skin/default/skin.css" id="skin" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/H-ui.admin/static/h-ui.admin/css/style.css" />
<!--[if IE 6]>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/H-ui.admin/lib/DD_belatedPNG_0.0.8a-min.js" ></script>
<script>DD_belatedPNG.fix('*');</script>
<![endif]-->
<title>类目列表</title>
</head>
<body>
	<nav class="breadcrumb">
		<i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 类目管理 <span class="c-gray en">&gt;</span> 类目列表 <a class="btn btn-success radius r" style="line-height: 1.6em; margin-top: 3px" href="javascript:location.replace(location.href);" title="刷新"><i class="Hui-iconfont">&#xe68f;</i></a>
	</nav>
	<div class="page-container">
		<div class="text-c">
			<!--
			<button onclick="removeIframe()" class="btn btn-primary radius">关闭选项卡</button>
			<span class="select-box inline"> <select name="" class="select">
					<option value="0">全部分类</option>
					<option value="1">分类一</option>
					<option value="2">分类二</option>
			</select>
			</span> 日期范围： <input type="text" onfocus="WdatePicker({ maxDate:'#F{$dp.$D(\'logmax\')||\'%y-%M-%d\'}' })" id="logmin" class="input-text Wdate" style="width: 120px;"> - <input type="text" onfocus="WdatePicker({ minDate:'#F{$dp.$D(\'logmin\')}',maxDate:'%y-%M-%d' })" id="logmax" class="input-text Wdate" style="width: 120px;"> <input type="text" name="" id="" placeholder=" 资讯名称" style="width: 250px" class="input-text">
			<button name="" id="" class="btn btn-success" type="submit">
				<i class="Hui-iconfont">&#xe665;</i> 搜资讯
			</button>
			-->
			<form action="NewsCat" method="get">
				<input type="hidden" name="oper" value="listDeal"> <span>名称：</span> <input type="text" class="input-text" id="catName" name="catName" value="${catName}" style="width: 250px;" placeholder="请输入名称">
				<button name="" id="" class="btn btn-success" type="submit">
					<i class="Hui-iconfont">&#xe665;</i> 搜索
				</button>
				<button name="" id="clearSearch" class="btn btn-success" type="button">
					<i class="Hui-iconfont">&#xe665;</i> 清空
				</button>
			</form>
		</div>
		<div class="cl pd-5 bg-1 bk-gray mt-20">
			<span class="l"><a href="javascript:;" onclick="datadel()" class="btn btn-danger radius"><i class="Hui-iconfont">&#xe6e2;</i> 批量删除</a> <a class="btn btn-primary radius" onclick="item_add('添加类目', 'NewsCat?oper=insert', '800', '500')" href="javascript:;"><i class="Hui-iconfont">&#xe600;</i> 添加类目</a></span> <span class="r">共有数据：<strong>${pagerItem.rowCount}</strong> 条
			</span>
		</div>
		<div class="mt-20">
			<table id="dataList" class="table table-border table-bordered table-bg table-hover table-sort table-responsive">
				<thead>
					<tr class="text-c">
						<th width="25"><input type="checkbox" name="" value=""></th>
						<th width="80">ID</th>
						<th width="230">名称</th>
						<th>描述</th>
						<th width="150">操作</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="item" items="${DataList}">
						<tr class="text-c">
							<td><input type="checkbox" value="${item.catId}" name=""></td>
							<td>${item.catId}</td>
							<td>${item.catName}</td>
							<td>${item.catDesc}</td>
							<td>
								<a style="text-decoration: none" class="ml-5" onClick="item_edit('查看[${item.catId}]','NewsCat?oper=detail&catId=${item.catId}','800','500')" href="javascript:;" title="查看">
									<i class="Hui-iconfont">&#xe720;</i>
								</a>
								<a style="text-decoration: none" class="ml-5" onClick="item_edit('编辑[${item.catId}]','NewsCat?oper=update&catId=${item.catId}','800','500')" href="javascript:;" title="编辑">
									<i class="Hui-iconfont">&#xe6df;</i>
								</a>
								<a style="text-decoration: none" class="ml-5" onClick="item_del(this,'${item.catId}')" href="javascript:;" title="删除">
									<i class="Hui-iconfont">&#xe6e2;</i>
								</a>
								<a style="text-decoration: none" class="ml-5" onClick="item_del_not_ajax(this,'${item.catId}')" href="javascript:;" title="非Ajax删除">
									<i class="Hui-iconfont">&#xe6e2;</i>
								</a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<jsp:include page="__pager.jsp" flush="true"></jsp:include>
		</div>
	</div>
	<!--_footer 作为公共模版分离出去-->
	<script type="text/javascript" src="${pageContext.request.contextPath}/static/H-ui.admin/lib/jquery/1.9.1/jquery.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/static/H-ui.admin/lib/layer/2.4/layer.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/static/H-ui.admin/static/h-ui/js/H-ui.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/static/H-ui.admin/static/h-ui.admin/js/H-ui.admin.js"></script>
	<!--/_footer 作为公共模版分离出去-->

	<!--请在下方写此页面业务相关的脚本-->
	<script type="text/javascript" src="${pageContext.request.contextPath}/static/H-ui.admin/lib/My97DatePicker/4.8/WdatePicker.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/static/H-ui.admin/lib/datatables/1.10.0/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/static/H-ui.admin/lib/laypage/1.2/laypage.js"></script>
	<script type="text/javascript">
		$(function() {
			$('#clearSearch').click(function() {
				location.href = 'NewsCat?oper=list';
			});
		});
		
		function item_add(title, url, w, h) {
			layer_show(title, url, w, h)
		}
		
		function item_del_not_ajax(obj, catId) {
			layer.confirm('确认要非ajax删除吗？', function(index) {
				location.href = 'NewsCat?oper=deleteNotAjax&catId=' + catId;
			});
		}
		
		function item_del(obj, catId) {
			layer.confirm('确认要删除吗？', function(index) {
				$.ajax({
					type: 'POST',
					url: 'NewsCat?oper=deleteDeal&catId=' + catId,
					success: function(data) {
						if (data == 'ok') {
							$(obj).parents('tr').remove();
							layer.msg('已删除！', {
								icon: 1,
								time: 1000
							});
						} else {
							layer.msg('删除失败！', {
								icon: 1,
								time: 1000
							});
						}
					},
					error: function(data) {
						console.log(data.msg);
					}
				});
			});
		}
		
		function datadel() {
			layer.confirm('确认要删除选中的数据吗？', function(index) {
				var num = 0;
				var total = 0;
				var obj = null;
				var catId = null;
				$('#dataList input[type=checkbox]:checked').each(function() {
					obj = this;
					catId = $(this).val();
					if (catId != null && catId != "0" && catId != "") {
						total++;
						$.ajax({
							type: 'POST',
							url: 'NewsCat',
							async: false,
							data: {
								oper: 'deleteDeal',
								catId: catId
							},
							success: function(data) {
								if (data == 'ok') {
									$(obj).parents('tr').remove();
									num++;
								}
							}
						});
					}
				});
				layer.msg('要删除' + total + '行记录，成功删除' + num + '行记录。', {
					icon: 1,
					time: 3000
				});
			});
		}
		
		function item_edit(title, url, w, h) {
			layer_show(title, url, w, h)
			
		}
		
		function item_detail(title, url, w, h) {
			layer_show(title, url, w, h)
			
		}
	</script>
</body>
</html>